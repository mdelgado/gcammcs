- Use XML cmd-line utility to query/modify an XML file:

xml ed --inplace --update '//world/region[@name="India"]/AgSupplySector/price-unit' -v '1975$/GJ' energy_crop_input-ORIGINAL.xml

To run a policy simulation, first run a baseline "calibration" run, then a policy run using the first run's output as input.
- Set CalibrationActive to 1:
	<Bools>
		...
		<Value name="CalibrationActive">1</Value>
		...	
	</Bools>

- Then run model to produce Output.xml

To run Policy case:
- Rename Output.xml from calibration step to, say, "CalibratedInput.xml"
- Perhaps specify a different output file name that's specific to this run
- Set CalibrationActive to 0
- Re-run the model

Other model outputs:
- main_log.txt (name set in log_conf.xml file)
- database.dbxml appended or created if needed (configuration tag: <Value name="xmldb-location")
- .dot files for use with Graphviz graph visualization software

Can use XML to define components of scenarios that are automatically combined to create
distinct scenarios, which are run automatically (see <BatchRunner>, <ComponentSet>, <FileSet>)
- User sets <Files> ... <value name="BatchFileName> ... </value> </Files> and
- <Bools> ... <value name="BatchMode">1</value> ... </Bools>

- �Main� scenario should not be seen as a most likely scenario, or even business
  as usual: it is only a starting point

====
Bioenergy
- What fraction of residue must be left on the ground?
- Electricity load is disaggregated (4 daily periods) only in the US
- Elec. load and generation in US are: off peak, intermed., sub peak, on peak
- Uses probabilistic formation to allow for heterogeneity. Market share based
  on prob that a technology has the lowest cost for an application. Uses Logit specification

====
Ag
- 12 crops, 6 animal categories, and bioenergy
- Bioenergy includes roundwood (forestry), switchgrass, miscanthus, jatropha, 
  willow, eucalyptus, corn ethanol, sugar ethanol, biodiesel (from soybeans and 
  other oil crops)
- Food and feed demand modeled at the 14-region level
- Per capita demand for crops, animals, and forestry products is fixed; grows with pop,
  regardless of income or price
- Meat demand is price responsive, but low elasticity of 0.25, consistent with US and Australia.
  Identified as a "conservative" assumption: presumes low meat-price elasticity in dev'g world.
- Crop demand is NOT price responsive
- GCAM results are production per year, not per harvest. Thus, GCAM uses total physical
  crop land area to calculate yield and not harvested area.  
- After 2050, GCAM assumes that yields will improve by 0.25% per year for all crops and regions.
=>INTERESTING TO MODEL YIELD DECLINE IN SOME REGIONS TO SEE HOW THIS AFFECTS RESULTS.
=>ALSO TO PLAY WITH PRICE ELASTICITY OF MEAT DEMAND IN DEV'G WORLD

====
Land Cover
- 151 regions
- 14 regions x 18 AEZx = 252, so not all occur
- Distribution of crops based on profit max, but probabilistically
- Yield is fixed in each subregion, but varies across regions (subregion = region-AEZ)
- Elasticities are not constant with logit formulation.
- Soil: change in C stock estimated each timestep; emissions are instant; sequestration takes time.

====
Emissions
- Tracks all key GHGs and aerosols
- Driven by �input� (e.g. fuel consumption) or �output� (e.g. service or energy production)
- Calculates CO2 from fossil fuel combustion and from LUC (HOW??)
- Default assumption is that LUC emissions are taxed same as fossil emissions
- Uses MAGICC 5.3 as climate model, converted FORTRAN=>C++, modified forcing of BC and OC
  - emissions data passed each time step
  - MAGICC computes concentrations and forcings, global mean T and sea level rise 

====
Energy Transformation
- Structure: supplysector / subsector / technology
- Subsector and technology market shares determined by two-level nested logit choice competition
  - Competition based of levelized cost of the energy service.

====
Final Demands
- Buildings, industry, transportation
====
Miscellany
- All energy flows are represented in EJ/yr. The �year� is implicit, not written out
  - Fuel carbon contents are in kgC/GJ
- CO2 is in Tg C (10^6 Mg). Multiply by 44/12 to convert to CO2.
- Non-CO2 gases are generally in Tg. Exceptions are the hi-GWP gases 
  (e.g. HFCs, PFCs, SF6), which are in Gg (same as kt).
-Prices of all energy goods and services are in 1975$/GJ
 - GDP is in 1990$/yr
 - Carbon prices are in 1990$/tC. Multiply by 12/44 to convert to 1990$/tCO2.

- Logit exponents control the degree of switching between technologies or fuels in response 
  to price changes
  - Low values = low fuel-switching = strong influence of base year shares even far into the future
  - High values tend towards winner take all-type response
====
Analysis
- Need to be able to plot lines for 2.5%, 50%, and 97.5% values for each trial to
  show the band of results over time, which should be a bit funnel shaped
- Discuss with Gouri how (if) GCAM could model a national LCFS policy, and perhaps
  the imposition of this policy in the US, EU and China simultaneously. 
- Use different strategies for assigning ILUC emissions and see result in delta T and
  concentration.
- See if anyone has used GCAM to model effect of decline in ag productivity in key regions 
   
====

Have a 2-pass operation that first checks that the paths all produce results
(or have this be an optional operation that reports the count for each path.

This finds all elements with ag supply sector starting with 'biomass'
tree.xpath("//AgSupplySector[starts-with(@name,'biomass')]")  also useful: contains(), ends-with()

Example of more complex query:
tree.xpath('//AgProductionTechnology[starts-with(@name, "eucalyptus")]//period[@year>2000 and @year<2020]')

Another example:
regexpNS = "http://exslt.org/regular-expressions"
find = etree.XPath("//*[re:test(., '^abc$', 'i')]", namespaces={'re':regexpNS})
  creates a callable object that takes an Element as an arg

If using multiple xpaths with the same document, it's more efficient to use this: 
XPathEvaluator(etree_or_element, namespaces=None, extensions=None, regexp=True, smart_strings=True)
   - Creates an XPath evaluator for an ElementTree or an Element.
   - The resulting object can be called with an XPath expression as argument
     and XPath variables provided as keyword arguments.
   - XPath class supports regex by default (can be turned off), but not sure how to use it.

>>> root = etree.XML("<root><a>aB</a><b>aBc</b></root>")
>>> print(find(root)[0].text)

Use ".//something" to find "something" at any level of the tree. This is what we need!
"bookstore//book/excerpt//emph" multiple "wildcard" directory levels are legal.
Useful examples: http://msdn.microsoft.com/en-us/library/ms256086.aspx
  many useful ways to query, by tag name, by attr name, with boolean expressions of matches
  
Modify the value:
factor = 1.5
first.text=str(float(first.text) * factor)

prices=reg1.xpath("supplysector/*/price")
print "of prices:", len(prices)

Note:
    try:
...   etree.parse(foo)
... except etree.XMLSyntaxError, e:
Also defined: 
   ParseError (synon. with above?)
   LxmlError  (base for all errors)
   XPathError (base for all XPath errors)
   
   
    Key methods of Element:
 get(key, default=None)    gets an element attribute
 getchildren()    deprecated: use list(element) or simply iterate over elements
 getparent()
 getroottree()
 find(path, namespaces=None) finds first match
 findall(path, namespaces=None) finds all matches
 findtext(path, default=None, namespaces=None)
    Finds text for the first matching subelement, by tag name or path.
 items(self)        gets element attributes as a sequence of (key, value) tuples 
 iter...            several iteration methods for children, parents, siblings, path matches
 keys(self)         gets a list of attribute names
 set(key, value)    sets an element attribute

 Properties (instance vars)
  tag
  text (None if none)

 Key methods of ElementTree:
 find, findall, findtext as above
 getroot        gets root element for this tree
 write(self, file, encoding=None, method="xml", pretty_print=False, 
       xml_declaration=None, with_tail=True, standalone=None, compression=0, 
       exclusive=False, with_comments=True, inclusive_ns_prefixes=None)
  - Write the tree to a filename, file or file-like object
  - Seems any value other than None causes XML declaration to be output (needed)

 xpath supports a sort of template, e.g., here with "name" variable
 expr = "//*[local-name() = $name]"
 >>> print(root.xpath(expr, name = "foo")[0].tag)
 foo
 
 
To apply a func to a list without returning values (as map would)
    filter(lambda x: func(x) and False, myList)