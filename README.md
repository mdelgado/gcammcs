# **gcammcs** -- GCAM Monte Carlo Simulation framework #

The `gcammcs` package is part of the Distributed Monte Carlo Simulation (Dist-MCS) framework. It provides all the tools needed to run simulations with GCAM on Linux clusters. The software is currently in alpha release.

`gcammcs` customizes the [coremcs](https://bitbucket.org/plevin/coremcs) package, which provides all the required model-independent functionality, to add support for the [GCAM](http://www.globalchange.umd.edu/models/gcam/) model. (See also the [gtapmcs](https://bitbucket.org/plevin/gtapmcs) package for use with the [GTAP](https://www.gtap.agecon.purdue.edu/) model.

This package requires the [coremcs](https://bitbucket.org/plevin/coremcs) package, in addition to the packages required by coremcs.

### How do I get set up? ###

Presently, there is no proper setup script. See the user documentation in `coremcs/doc/Distributed-MCS-guide-v1.0.pdf`

### Contribution guidelines ###

* Later for that... in the meantime, contact me (see below.)

### Who do I talk to? ###

* Contact Richard Plevin (rich@plevin.com) for more information