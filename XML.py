'''
Created on July 5, 2013

@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.

Note: Useful doc: http://infohost.nmt.edu/tcc/help/pubs/pylxml/web/index.html
'''
from cStringIO import StringIO
from lxml import etree as ET

import coremcs.log
from coremcs.error import CoreMcsUserError

_logger = coremcs.log.getLogger(__name__)


def findAndSave(elt, eltName, cls, myDict, tree=None):
    '''
    Simple helper function to find elements with a given name, create
    a wrapper instance and store them in the given dictionary.
    '''
    for node in elt.iterfind(eltName):
        name = node.get('name')
        obj  = cls(node, tree) if tree else cls(node)
        if name in myDict:
            raise CoreMcsUserError("Attempt to save %s instance with duplicate name '%s'" \
                    % (eltName, name))
        myDict[name] = obj


class XMLWrapper(object):
    '''
    A simple wrapper around any ElementTree element.
    '''
    def __init__(self, element):
        self.element = element

    def getElement(self):
        return self.element

    def elementName(self):
        return self.element.get('name')


class XMLFile(object):
    '''
    Stores information about an XML file. Provide wrapper to parse and access
    the file tree.
    '''
    def __init__(self, filename, load=True):
        self.filename = filename
        self.tree = None

        if filename and load:
            self.read()

    def getRoot(self):
        return self.tree.getroot()

    def getTree(self):
        'Returns XML parse tree.'
        return self.tree

    def getFilename(self):
        return self.filename

    def read(self, filename=None):
        """
        Read the XML file, and if validate is not False and getDTD()
        returns a non-False value, validate the XML against the returned DTD.
        """
        if filename:
            self.filename = filename

        _logger.info("Reading '%s'", self.filename)
        parser = ET.XMLParser(remove_blank_text=True)

        try:
            self.tree = ET.parse(self.filename, parser)

        except Exception as e:
            raise CoreMcsUserError("Can't read XML file '%s': %s" % (self.filename, e))

        dtd = self.getDTD()
        if dtd:
            self.validateXML(dtd)

        return self.tree

    def getDTD(self):
        """
        Subclasses can implement this to return a string containing the DTD,
        which causes the XML file to be validated in the read() method.
        """
        return None

    def validateXML(self, dtdStr, raiseOnError=True):
        """
        Validate a ParameterList against dtdStr, a string representation
        of a DTD. Optionally raises an error on failure, else return boolean
        validity status.
        """
        dtdObj = ET.DTD(StringIO(dtdStr))
        valid = dtdObj.validate(self.getRoot())

        if raiseOnError and not valid:
            raise CoreMcsUserError(dtdObj.error_log.filter_from_errors()[0])

        return valid
