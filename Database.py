"""
Created on Sep 29, 2013

@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.
"""
from operator import itemgetter
from sqlalchemy import Table, Column, Integer, Float, String, ForeignKey, Index
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import load_only

import coremcs.log
from coremcs.error import CoreMcsUserError
from coremcs.Database import (CoreDatabase, Run, Input, InValue, Output, Experiment,
                              CoreMCSMixin, ORMBase)
from coremcs.Configuration import getConfigManager

# TODO: might be useful:
# pd.read_sql_table('data', engine, columns=['Col_1', 'Col_2'])
# pd.read_sql_table('data', engine, index_col='id')
# pd.read_sql_table('data', engine, parse_dates=['Date'])
# pd.read_sql_table('data', engine, parse_dates={'Date': '%Y-%m-%d'})

_logger = coremcs.log.getLogger(__name__)

# Default region map for GCAM 4.
# TBD: Allow user to override by specifying a mapping file in the config file (parameter GCAM.RegionMapFile)?
RegionMap = {
    'Multiple': -1,
    'global': 0,
    'USA': 1,
    'Africa_Eastern': 2,
    'Africa_Northern': 3,
    'Africa_Southern': 4,
    'Africa_Western': 5,
    'Australia_NZ': 6,
    'Brazil': 7,
    'Canada': 8,
    'Central America and Caribbean': 9,
    'Central Asia': 10,
    'China': 11,
    'EU-12': 12,
    'EU-15': 13,
    'Europe_Eastern': 14,
    'Europe_Non_EU': 15,
    'European Free Trade Association': 16,
    'India': 17,
    'Indonesia': 18,
    'Japan': 19,
    'Mexico': 20,
    'Middle East': 21,
    'Pakistan': 22,
    'Russia': 23,
    'South Africa': 24,
    'South America_Northern': 25,
    'South America_Southern': 26,
    'South Asia': 27,
    'South Korea': 28,
    'Southeast Asia': 29,
    'Taiwan': 30,
    'Argentina': 31,
    'Colombia': 32
}

RegionAliases = {
    'all regions': 'global',
    'rest of world': 'multiple',
    'row': 'multiple'
}

# The name of the program as stored in the "program" table
GCAM_PROGRAM = 'gcam'
YEAR_COL_PREFIX = 'y'

# class GCAMExperiment(Experiment):
#     '''
#     An extended version of coremcs' Experiment, manually mapped to the table 'experiment'
#     '''
#     def __init__(self, expId, expName, description, parent):
#         super(GCAMExperiment).__init__(self, expId, expName, description)
#         self.parent = parent


# Map region numbers to region names. Both ids and names must be unique.
class Region(CoreMCSMixin, ORMBase):
    regionId = Column(Integer, primary_key=True, autoincrement=False)
    canonName = Column(String)        # canonical name for lookup (lowercase, "_" changed to " ")
    displayName = Column(String)        # display name
    __table_args__ = (Index("region_index1", "canonName", unique=True),)


class TimeSeries(CoreMCSMixin, ORMBase):
    '''
    N.B. Columns representing the years of interest are added dynamically as
    "y2005", "y2010", etc.
    '''
    seriesId = Column(Integer, primary_key=True)
    runId = Column(Integer, ForeignKey('run.runId', ondelete="CASCADE"))
    regionId = Column(Integer, ForeignKey('region.regionId', ondelete="CASCADE"))
    outputId = Column(Integer, ForeignKey('output.outputId', ondelete="CASCADE"))
    units = Column(String)


def canonicalizeRegion(name):
    '''
    :param name: a GCAM region name
    :return: region name in canonical format, i.e., lowercase with underscores changed to spaces
    (The use of underscores is inconsistent and thus hard to remember, e.g., 'South America_Northern')
    '''
    name = name.lower()
    if name in RegionAliases:
        name = RegionAliases[name]

    return name.replace('_', ' ')


class GcamDatabase(CoreDatabase):
    _yearColsAdded = False
    _expColsAdded = False

    def __init__(self):
        super(GcamDatabase, self).__init__()
        self.paramIds = {}                   # parameter IDs by name
        self.canonicalRegionMap = {}

        # Cache these to avoid database access in saveResults loop
        for regionName, regionId in RegionMap.iteritems():
            canonName = canonicalizeRegion(regionName)
            self.canonicalRegionMap[canonName] = regionId

    def initDb(self, args=None):
        'Add GCAM-specific tables to the coremcs database'
        super(GcamDatabase, self).initDb(args=args)

        self.addYearCols()
        self.addExpCols()

        if args and args.empty:
            return

        self.addRegions(RegionMap)

    def startDb(self, checkInit=True):
        super(GcamDatabase, self).startDb(checkInit=checkInit)
        self.addYearCols(alterTable=False)
        self.addExpCols(alterTable=False)

    def createExp(self, name, parent=None, description=None):
        '''
        Insert a row for the given experiment. Replaces superclass method
        to add 'parent' argument.
        '''
        session = self.Session()
        exp = Experiment(expId=None, expName=name, description=description)
        exp.parent = parent # not in Experiment's __init__ signature
        session.add(exp)
        session.commit()
        expId = exp.expId
        session.close()
        return expId

    def updateExp(self, name, parent=None, description=None):
        session = self.Session()
        exp = self.getExp(name, session=session)
        exp.description = description
        exp.parent = parent
        session.commit()
        session.close()

    def getExp(self, expName, session=None, raiseError=True):
        sess = session or self.Session()

        try:
            exp = sess.query(Experiment).filter_by(expName=expName).one()

        except NoResultFound:
            msg = "The experiment '%s' is not defined" % expName
            if raiseError:
                raise CoreMcsUserError(msg)
            else:
                _logger.info(msg)
                exp = None

        finally:
            if not session:
                sess.close()

        return exp

    # TBD: should filter by program name, too
    def getExpParent(self, expName, session=None):
        sess = session or self.Session()

        exp = self.getExp(expName, sess)
        parent = exp.parent

        if not session:
            sess.close()

        return parent

    # TBD: generalize and add to coremcs.Database since often modified
    def addExpCols(self, alterTable=True):
        '''
        Add required columns to the Experiment table.
        '''
        if self._expColsAdded:
            return

        session = self.Session()
        engine = session.get_bind()
        meta = ORMBase.metadata
        meta.bind = engine
        meta.reflect()
        expTable = Experiment.__table__

        cols = [('parent', String)]

        for (colName, colType) in cols:
            if colName not in expTable.columns:
                column = Column(colName, colType)
                if alterTable:
                    self.addColumns(Experiment, column)
                else:
                    setattr(Experiment, column.name, column)        # just add the mapping

        self._expColsAdded = True
        session.close()

    @staticmethod
    def yearCols():
        # Create the time series table with years (as columns) specified in the config file.
        cfg = getConfigManager()
        years = cfg.activeYears()
        cols = map(lambda y: YEAR_COL_PREFIX + y, years)
        return cols

    def addYearCols(self, alterTable=True):
        '''
        Define year columns (y1990, y2005, y2010, etc.) dynamically. If alterTable
        is True, the SQL table is altered to add the column; otherwise the column
        is just mapped to an attribute of the TimeSeries class.
        '''
        if self._yearColsAdded:
            return

        session = self.Session()
        engine = session.get_bind()
        meta = ORMBase.metadata
        meta.bind = engine
        meta.reflect()

        colNames = self.yearCols()

        timeSeries = Table('timeseries', meta, autoload=True, autoload_with=engine)

        # Add columns for all the years used in this analysis
        for colName in colNames:
            if colName not in timeSeries.columns:
                column = Column(colName, Float)
                if alterTable:
                    self.addColumns(TimeSeries, column)
                else:
                    setattr(TimeSeries, column.name, column)        # just add the mapping

        self._yearColsAdded = True
        session.close()

    def addRegions(self, regionMap):
        # TBD: read region map from file identified in config file, or use default values
        # For now, use default mapping
        session = self.Session()

        for name, regId in regionMap.iteritems():
            self.addRegion(regId, name, session=session)

        session.commit()
        session.close()

    def addRegion(self, regionId, name, session=None):
        sess = session or self.Session()
        obj = Region(regionId=regionId, displayName=name, canonName=canonicalizeRegion(name))
        sess.add(obj)

        if session:         # otherwise, caller must commit
            sess.commit()
            sess.close()

    def getRegionId(self, name):
        canonName = canonicalizeRegion(name)
        regionId = self.canonicalRegionMap[canonName]
        return regionId

    def getParamId(self, pname):
        return self.paramIds[pname]

    def createOutput(self, name, program=GCAM_PROGRAM, description=None, session=None):
        _logger.debug("createOutput(%s)", name)
        return super(GcamDatabase, self).createOutput(name, program=program, description=description, session=session)

    def saveParameterNames(self, tuples):
        '''
        Define parameter names in the database on the fly based on results of XPath queries.
        "tuples" is a list of (paramName, description) pairs.
        '''
        session = self.Session()
        programId = self.getProgramId(GCAM_PROGRAM)

        # TBD: The following code is subject to a race condition, but we don't expect multiple users to
        # TBD: generate simulations in the same model run dir simultaneously. If they do, this may break.
        # TBD: Could handle this with a lock...
        pnames = map(itemgetter(0), tuples)
        rows = session.query(Input).filter(Input.programId == programId, Input.paramName.in_(pnames)).all()
        found = map(lambda row: row.paramName, rows)

        descByName = dict(tuples)
        notFound = set(pnames) - set(found)

        # Construct list of tuples holding only the parameters that were
        # found, and whose description changed. These are updated automatically
        # when the session is committed.
        updTuples = []
        for row in rows:
            desc = descByName[row.paramName]
            if row.description != desc:
                row.description = desc
                updTuples.append(row)

        # Create a list of objects that need to be inserted, then add them all at once
        newInputs = map(lambda name: Input(programId=programId, paramName=name, description=descByName[name]), notFound)
        session.add_all(newInputs)

        # Insert new parameter descriptions for these
        session.commit()

        # Cache all parameter IDs for faster lookup
        rows = session.query(Input.inputId, Input.paramName).all()
        for row in rows:
            self.paramIds[row.paramName] = row.inputId

        session.close()

    def saveParameterValues(self, simId, tuples):
        '''
        Save the value of the given parameter in the database. Tuples are
        of the format: (trialNum, paramId, value, varNum)
        '''
        session = self.Session()
        qsession = self.Session()

        for trialNum, paramId, value, varNum in tuples:
            # We save varNum to distinguish among independent values for the same variable name.
            # The only purpose this serves is to ensure uniqueness, enforced by the database.
            inValue = InValue(inputId=paramId, simId=simId, trialNum=trialNum,
                              value=value, row=0, col=varNum)
            session.add(inValue)

        session.commit()
        session.close()
        qsession.close()

    def deleteRunResults(self, runId, outputIds=None, session=None):
        """
        Augment core method by deleting timeseries data, too.
        """
        _logger.debug("deleteRunResults: deleting results for runId %d, outputIds=%s" % (runId, outputIds))
        sess = session or self.Session()
        super(GcamDatabase, self).deleteRunResults(runId, outputIds=outputIds, session=sess)

        query = sess.query(TimeSeries).filter_by(runId=runId)

        if outputIds:
            query = query.filter(TimeSeries.outputId.in_(outputIds))

        query.delete(synchronize_session='fetch')

        if session is None:
            self.commitWithRetry(sess)
            sess.close()

    def saveTimeSeries(self, runId, regionId, paramName, values, units=None, session=None):
        sess = session or self.Session()

        programId = self.getProgramId(GCAM_PROGRAM)

        # one() raises error if 0 or more than 1 row is found, otherwise returns a tuple.
        try:
            row = sess.query(Output).filter(Output.programId == programId, Output.name == paramName).one()
        except Exception:
            _logger.error("Can't find param %s for %s", paramName, GCAM_PROGRAM)
            raise

        outputId = row.outputId

        ts = TimeSeries(runId=runId, outputId=outputId, regionId=regionId, units=units)

        for name, value in values.iteritems():  # Set the values for "year" columns
            setattr(ts, name, value)

        sess.add(ts)

        if not session:
            sess.commit()
            sess.close()

    def getTimeSeries(self, simId, paramName, expName):
        '''
        Retrieve all timeseries rows for the given simId and paramName.

        :param simId: simulation ID
        :param paramName: name of output parameter
        :param expName: the name of the experiment to select results for
        :return: list of TimeSeries tuples or None
        '''
        session = self.Session()

        cols = ['seriesId', 'runId', 'outputId', 'units'] + self.yearCols()

        # NB: 'alarmed' runs may have saved some results, so we only load 'succeeded'
        query = session.query(TimeSeries).options(load_only(*cols)). \
            join(Run).filter_by(simId=simId).filter_by(status='succeeded'). \
            join(Experiment).filter_by(expName=expName). \
            join(Output).filter_by(name=paramName)

        rslt = query.all()
        session.close()
        return rslt
