#!/usr/bin/env python
import sys
import os
from shutil import rmtree

def quitWithMsg(msg):
    print 'epilogue.py:', msg
    sys.exit(1)

# print "epilogue.py: script args: %s\n" % sys.argv

jobId = sys.argv[1]

# Grab the numbers before first '.', e.g., in 417088.ext-6-0.evergreen.umd.edu
jobNum = jobId.split('.')[0]

# Find the symlink that points to the output dir associated with this job. It's
# created in user's home dir to avoid leaving cruft on compute node /tmp dirs.
tmpDir   = os.path.expanduser('~/.mcs/tmp')
linkName = '%s/mcs.%s.link' % (tmpDir, jobNum)

#
# See if there's a link associated with this jobNum. This will be
# the case if we're re-running "queue" after with an existing run,
# e.g., to re-run batch queries or post-processor. (Mostly in testing.)
#
if not os.path.exists(linkName):
    quitWithMsg("Symlink %s does not exist" % linkName)

if not os.path.islink(linkName):
    quitWithMsg("File %s should be a symlink, but it isn't" % linkName)

# Read through link to get real directory
dirName = os.path.realpath(linkName)

if not os.path.isdir(dirName):
    quitWithMsg('Symlink %s points to non-existent target %s' % (linkName, dirName))

removeFiles = True

if removeFiles:
    print "epilogue.py: removing %s and %s" % (dirName, linkName)
    rmtree(dirName)
    os.unlink(linkName)
else:
    print 'epilogue.py: Would do:'
    print 'rmtree(%s)' % dirName
    print 'unlink(%s)' % linkName

sys.exit(0)
