'''
Created on 10/2/14

@author: rjp
'''
import sys
from gcammcs.GCAM import runGCAM

# Define function so Runner.py can import and run without spawning a separate process
def runner(args):
    return runGCAM(args)

if __name__ == '__main__':
    runner(sys.argv)
