#!/usr/bin/env python2
'''
@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.
'''
#
# Main script to run gcammcs package
#
import os
import sys

# Read the following imports from the same dir as the script or from
# the parent of that directory, which handles the case of mcs/bin/xxx
_dirname = os.path.dirname(sys.argv[0])
sys.path.insert(0, _dirname)
sys.path.insert(0, os.path.dirname(_dirname))

from coremcs.Package import runPackage
from gcammcs.Package import GcamPackage

runPackage(GcamPackage)
