'''
Created on 10/25/14

@author : Rich Plevin

@Copyright 2015 Richard Plevin
'''
import coremcs.error

class GcamMcsSystemError(coremcs.error.CoreMcsSystemError):
    pass

class GcamMcsUserError(coremcs.error.CoreMcsUserError):
    'The user provided an incorrect parameter or failed to provide a required one.'
    pass
