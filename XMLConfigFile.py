"""
@author: Richard Plevin

Copyright (c) 2015 Richard Plevin. See the file COPYRIGHT.txt for details.
"""
import os
from lxml import etree as ET

import coremcs.log
from coremcs.error import CoreMcsUserError
from .XML import XMLFile
from .util import getSimConfigFile

_logger = coremcs.log.getLogger(__name__)

# The pre-defined sections of GCAM configuration files
COMPONENTS_GROUP = 'ScenarioComponents'
FILES_GROUP   = 'Files'
STRINGS_GROUP = 'Strings'
INTS_GROUP    = 'Ints'
BOOLS_GROUP   = 'Bools'
DOUBLES_GROUP = 'Doubles'

# TBD: create interface to load "most local" version of config file.
class XMLConfigFile(XMLFile):
    '''
    Stores information about a GCAM config.xml file
    '''
    instances = {}  # XMLConfigFile instances keyed by scenario name

    def __init__(self, simId, scenario):
        '''
        Read and cache a GCAM configuration file in self.tree.
        '''
        pathname = getSimConfigFile(simId, scenario)
        super(XMLConfigFile, self).__init__(pathname)

        # Default writePath is where we were read from.
        self.writePath = pathname
        self.scenario  = scenario

    @classmethod
    def getConfigForScenario(cls, simId, scenario):
        '''
        Return the path to the run-tree version of the config file
        for the given scenario.
        '''
        try:
            return cls.instances[scenario]

        except KeyError:
            obj = XMLConfigFile(simId, scenario)
            cls.instances[scenario] = obj
            return obj

    @classmethod
    def writeAll(cls, simId):
        from gcammcs.util import getSimConfigFile

        for obj in cls.instances.values():
            configPath = getSimConfigFile(simId, obj.scenario)
            obj.write(path=configPath)

    def write(self, path=None):
        '''
        Write out the modified configuration file tree to the
        same path that it was read from, or to the given path.
        '''
        if path:
            self.writePath = path       # remember the new path if one is given
        else:
            path = self.writePath

        if os.path.exists(path):        # remove it since it might be a symlink and
            os.unlink(path)             # we don't want to write through to the src

        _logger.debug("XMLConfigFile writing %s", path)
        self.tree.write(path, xml_declaration=True, pretty_print=True)

    def getConfigElement(self, name, group):
        '''
        Find the config file component with the specified tag and return
        the corresponding Element.
        '''
        xpath = '//Configuration/%s/Value[@name="%s"]' % (group, name)
        found = self.tree.xpath(xpath)
        return None if len(found) == 0 else found[0]

    def getConfigValue(self, name, group):
        elt = self.getConfigElement(name, group)
        return None if elt is None else elt.text

    def getConfigGroup(self, group):
        groupElt = self.tree.xpath('//Configuration/%s' % group)
        if groupElt is None:
            raise CoreMcsUserError('getConfigGroup: group "%s" was not found' % group)

        return groupElt[0]

    def updateConfigElement(self, name, group, newName=None, newValue=None):
        '''
        Update the name attribute, the value (element text), or both for the named config element.
        '''
        if not (newValue or newName):
            raise CoreMcsUserError('updateConfigElement: must provide newTag or text, or both')

        elt = self.getConfigElement(name, group)
        if elt is None:
            raise CoreMcsUserError('updateConfigElement: element "%s" was not found in group %s' % (name, group))

        if newName:
            elt.set('Name', newName)

        if newValue:
            elt.text = str(newValue)    # in case it was passed as numeric or bool

    def getComponentPathname(self, name):
        pathname = self.getConfigValue(name, COMPONENTS_GROUP)
        if not pathname:
            raise CoreMcsUserError('getComponentPathname: a file with tag "%s" was not found' % name)
        return pathname

    def updateComponentPathname(self, name, pathname):
        self.updateConfigElement(name, COMPONENTS_GROUP, newValue=pathname)

    def addConfigElement(self, name, group, value):
        '''
        Append a new element with the given name and value to the given group.
        '''
        groupElt = self.getConfigGroup(group)
        elt = ET.SubElement(groupElt, 'Value', name=name)
        elt.text = value
        return elt

    def deleteConfigElement(self, name, group):
        '''
        Remove the Value element with the given name from the given group
        '''
        groupElt = self.getConfigGroup(group)
        elt = self.getConfigElement(name, group)
        if elt is None:
            raise CoreMcsUserError('deleteConfigElement: element "%s" was not found in group %s' % (name, group))

        groupElt.remove(elt)
