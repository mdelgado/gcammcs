'''
Created on 6/22/15

@author: rjp
'''
import os
from coremcs.Configuration import getConfigManager
from coremcs.util import getSimDir

# Basenames of common files
ConfigFileName    = "config.xml"
ScenarioFileName  = "scenarios.xml"
ParameterFileName = "parameters.xml"
ResultFileName    = "results.xml"
QueryDirName      = "queries"

SimAppXmlDirName   = "app-xml"
SimLocalXmlDirName = "local-xml"

def getSimXmlDir(simId):
    """
    Returns the top-level simulation-specific App xml dir, e.g., {simDir}/app-xml
    """
    simDir = getSimDir(simId)
    path = os.path.join(simDir, SimAppXmlDirName)
    return path

def getSimXmlFile(simId, filename):
    """
    Returns the path to a file in the sim's XML dir, e.g., {simDir}/app-xml/foo.xml
    """
    xmlDir = getSimXmlDir(simId)
    path = os.path.join(xmlDir, filename)
    return path

def getSimParameterFile(simId):
    """
    Returns the path to sim's copy of the parameters.xml file.
    """
    return getSimXmlFile(simId, ParameterFileName)

def getSimScenarioFile(simId):
    """
    Returns the path to sim's copy of the scenarios.xml file.
    """
    return getSimXmlFile(simId, ScenarioFileName)

def getSimResultFile(simId):
    """
    Returns the path to sim's copy of the results.xml file.
    """
    return getSimXmlFile(simId, ResultFileName)

def getSimLocalXmlDir(simId):
    """
    Returns the path to sim's local-xml dir.
    """
    simDir = getSimDir(simId)
    path = os.path.join(simDir, SimLocalXmlDirName)
    return path

def getSimScenarioDir(simId, scenarioName):
    cfg = getConfigManager()
    subdir = cfg.getParam('GCAM.ScenarioSubdir')

    localXmlDir = getSimLocalXmlDir(simId)
    scenDir = os.path.join(localXmlDir, subdir, scenarioName)
    return scenDir

def getSimConfigFile(simId, scenarioName):
    """
    Returns the path to sim's copy of the config.xml file for the given scenario.
    """
    scenDir = getSimScenarioDir(simId, scenarioName)
    configFile = os.path.join(scenDir, ConfigFileName)
    return configFile

def getRunQueryDir():
    """
    Returns the path to sim's copy of the scenarios.xml file.
    """
    cfg = getConfigManager()
    workspace = cfg.getParam('GCAM.RunWorkspace')

    path = os.path.join(workspace, QueryDirName)
    return path
