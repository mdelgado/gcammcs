"""
Created on 10/25/14

@author: Richard Plevin

Copyright (c) 2014-2015 Richard Plevin. See the file COPYRIGHT.txt for details.
"""
import os.path

import coremcs.log
from coremcs.error import CoreMcsUserError, CoreMcsSystemError
from coremcs.util import filecopy
from .XML import XMLFile
from .XMLConfigFile import XMLConfigFile, FILES_GROUP
from .util import ConfigFileName, getSimScenarioFile, getSimScenarioDir

_logger = coremcs.log.getLogger(__name__)

# EXAMPLE
#
# <Scenarios>
#     <!-- "type" may not be necessary as it can be inferred -->
#     <Scenario name="new-reference" type="reference">
#         <!-- No parent scenario for reference scenarios -->
#         <Generator>common/setupReference.sh</Generator>
#     <_Scenario>
#
# 	<Scenario name="base-1" type="baseline" parent="new-reference">
#         <Generator>EPA/baseline/setupBase-1.sh</Generator>
# 	<_Scenario>
#
# 	<Scenario name="corn-1" parent="base-1">
#         <Generator>EPA/corn/setupCorn-1.sh</Generator>
# 	<_Scenario>
#
# 	_Scenario name="stover-1" parent="base-1" active="0">
#         <Generator>EPA/stover/setupCorn-1.sh</Generator>
# 	<_Scenario>
# </Scenarios>


SCENARIO_ELT_NAME  = 'Scenario'
GENERATOR_ELT_NAME = 'Generator'
DESC_ELT_NAME      = 'Description'

SCENARIOS_DTD = '''
    <!ELEMENT Scenarios (Scenario+)>

    <!ELEMENT Scenario ((Generator?,Description?)|(Description?,Generator?))>
    <!ATTLIST Scenario
      name   CDATA #REQUIRED
      parent CDATA #IMPLIED
      type   (reference|baseline|derived) "derived"
      active (0|1) "1">

    <!ELEMENT Generator (#PCDATA)>
    <!ELEMENT Description (#PCDATA)>
'''

class _Scenario(object):
    '''
    Private class that stores information about a single scenario and provides
    scenario creation and manipulation functions required by GCAM-MCS.
    '''
    instances = dict()

    def __init__(self, elt, simId):
        self.simId = simId
        self.element = elt
        self.name = elt.get('name')
        self.type = elt.get('type', 'derived')
        self.parentName = elt.get('parent')
        self.parent = None
        self.active = int(elt.get('active', '1'))   # active by default
        self.generator = None
        self.scenDir = getSimScenarioDir(simId, self.name)
        self.configFile = None
        self.configPath = None

        genElt = elt.find(GENERATOR_ELT_NAME)       # DTD requires zero or one Generator subnode
        self.generator = genElt.text if genElt is not None else None

        descElt = elt.find(DESC_ELT_NAME)           # Description is optional; there are either 0 or 1 of these.
        self.description = descElt.text if descElt is not None else None

        self.instances[self.name] = self            # record all instances to allow lookup by name

    @classmethod
    def find(cls, name):
        try:
            return cls.instances[name]

        except KeyError:
            raise CoreMcsUserError("Unknown scenario '%s'" % name)

    @classmethod
    def getScenarios(cls, which='active'):
        """
        Return a list of scenario objects. If which is 'all', all scenarios are
        returned. If which is 'active', only active scenarios are returned. If
        which is 'inactive', only inactive scenarios are returned. Other values
        cause CoreMcsUserError to be raised.
        """
        all = _Scenario.instances.values()
        if which == 'all':
            return all

        if which == 'active':
            return filter(cls.isActive, all)

        if which == 'inactive':
            return filter(lambda s: not s.isActive(), all)

        raise CoreMcsUserError('Invalid value for "which" parameter of getScenarios: %s' % which)

    @classmethod
    def getScenarioNames(cls, which='active'):
        scenarios = cls.getScenarios(which=which)
        names = map(cls.getName, scenarios)
        return names

    def __str__(self):
        return "<_Scenario name='%s' dir='%s' type='%s' parent='%s' active='%d'>" % \
               (self.name, self.scenDir, self.type, self.parent, self.active)

    def getName(self):
        return self.name

    def getParentName(self):
        return self.parentName

    def setParent(self, obj):
        self.parent = obj

    def getParent(self):
        return self.parent

    def getType(self):
        return self.type

    def getDescription(self):
        return self.description

    def isActive(self):
        return self.active

    def getScenarioConfigPath(self):
        pathname = os.path.join(self.scenDir, self.name, ConfigFileName)
        return pathname

    def getLocalConfig(self):
        '''
        Copy the nearest config file definition to local directory
        if needed, load and cache the XML to edit it as needed.
        '''
        if self.configFile:
            return self.configPath

        configPath = self.getScenarioConfigPath()

        if not os.path.lexists(configPath):
            # copy parent's config to local scenario dir
            parent = self.getParent()
            parentConfigPath = parent.getLocalConfig()
            filecopy(parentConfigPath, configPath)

        # Load the XML
        self.configFile = XMLConfigFile.getInstance(configPath)
        self.configPath = configPath
        return configPath

    # Deprecated, or not needed until scenario generation methods in place?
    def getLocalCopy(self, fileTag):
        '''
        Get a local copy of the file indicated by "tag", which must
        refer to a scenario element defined in the local config file.
        '''
        self.getLocalConfig()   # ensure there's a local copy of config file
        relPath = self.configFile.getConfigValue(fileTag, FILES_GROUP)
        if not relPath:
            raise CoreMcsUserError("A config file element with tag '%s' was not found" % fileTag)

        scenName = self.getName()
        basename = os.path.basename(relPath)
        localName = os.path.join(scenName, basename)
        _logger.debug("_Scenario.getLocalCopy: local name is '%s'", localName)

        if not os.path.lexists(localName):
            absPath = os.path.abspath(os.path.join(scenName, relPath))
            #filecopy(absPath, localName)
            _logger.info("WOULD (but didn't) copy %s to %s", absPath, localName)

        # TBD: copy this file to the local scenario dir
        # TBD: convert relative path (text of fileElt) to relative to current dir

    @classmethod
    def generateScenario(cls, name, outputDir, execute=True):
        scenario = cls.find(name)
        try:
            scenario.runGenerator(outputDir, execute=execute)

        except Exception, e:
            _logger.fatal("Can't run scenario '%s': %s", name, e)
            raise

    def addExperiment(self, configFile):
        from coremcs.Database import getDatabase        # lazy imports
        from sqlalchemy.exc import IntegrityError

        db = getDatabase()
        desc   = self.getDescription() or ('Defined in ' + configFile)
        parent = self.getParentName()

        try:
            db.createExp(self.name, description=desc, parent=parent)

        except IntegrityError:
            db.updateExp(self.name, description=desc, parent=parent)

        except Exception, e:
            raise CoreMcsSystemError("Failed to create experiment: %s", e)


class XMLScenarioFile(XMLFile):
    '''
    Support for generating and running GCAM scenarios.
    '''

    def __init__(self, pathname=None, simId=None):
        #cfg = getConfigManager()
        #cfg.getParam('GCAM.UserScenarioFile')
        pathname = pathname or getSimScenarioFile(simId)

        super(XMLScenarioFile, self).__init__(pathname)

        root = self.getRoot()

        # instances are stored in _Scenario class variable
        scenarios = map(lambda elt: _Scenario(elt, simId), root.findall(SCENARIO_ELT_NAME))
        _logger.debug("Scenarios defined in %s: %s", pathname, map(_Scenario.getName, scenarios))

    def getDTD(self):
        return SCENARIOS_DTD

    def addExperiments(self):
        for s in _Scenario.instances.values():
            s.addExperiment(self.filename)

    @staticmethod
    def getScenarioNames(which='active'):
        """
        Just a pass-thru so we can keep the _Scenario class private.
        """
        return _Scenario.getScenarioNames(which=which)

    @staticmethod
    def getGenerator(scenName):
        scen = _Scenario.find(scenName) # throws exception if not found
        return scen.generator

    @staticmethod
    def getParentName(scenName):
        scen = _Scenario.find(scenName) # throws exception if not found
        parentName = scen.getParentName()
        return parentName


if __name__ == '__main__':
    # Simple test code
    pathname = '/Users/rjp/bitbucket/mcs/gcammcs/template/app/_appName_/programs/gcam/scenarios/new-scenarios-ref.xml'
    targetDir = 'test/Scenarios/test'
    scenFile = XMLScenarioFile(pathname)
    _Scenario.generateScenario('test', targetDir)

# TBD: use this in a unit test
# if False:
#     from gcammcs.Package import GcamConfiguration
#     getConfigManager(GcamConfiguration)
#
#     configPath = '/Users/rjp/tmp/test/config-test.xml'
#     configObj  = XMLConfigFile.getInstance(configPath)
#     root = configObj.getRoot()
#     #ET.dump(root, pretty_print=True)
#     configObj.updateConfigElement('stop-period', INTS_GROUP, newValue=11)
#
#     configObj.deleteConfigElement('outFileName', FILES_GROUP)
#     configObj.addConfigElement('NewFile', COMPONENTS_GROUP, '../../trial-xml/foo.xml')
#
#     #print "\nUpdated configuration:\n"
#     #ET.dump(root, pretty_print=True)
#     configObj.write(path='/Users/rjp/tmp/new-config.xml')
#
#     #scen = _Scenario('test', '/Users/rjp/tmp')
