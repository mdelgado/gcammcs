'''
@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.
'''
import os
from shutil import copytree, rmtree, ignore_patterns

from coremcs import log
from coremcs.error import CoreMcsUserError, CoreMcsSystemError
from coremcs.Package import CorePackage, NEWAPP_CMD, GENSIM_CMD, QUEUE_CMD, ANALYZE_CMD
from coremcs.Configuration import CoreConfiguration, getConfigManager
from coremcs.util import getSimDir, getTrialDir, rmlink, symlink, filecopy, mkdirs, saveDict, parseTrialString

from .Database import GcamDatabase, GCAM_PROGRAM

_logger = log.getLogger(__name__)

SCENARIO_CMD = "scenario"
DIFF_CMD = "diff"
COPY_XML_CMD = "copyxml"
ADD_OUTPUTS_CMD = 'addoutputs'
PARAM_FILENAME = "parameters.xml"


def copyDir(srcDir, dstDir):
    '''
    Recursively copy srcDir to dstDir, removing dstDir first in case it's a symlink.
    '''
    if os.path.isdir(dstDir):
        _logger.debug('Removing %s', dstDir)
        rmtree(dstDir)

    _logger.debug('Copying %s to %s', srcDir, dstDir)
    copytree(srcDir, dstDir, symlinks=False, ignore=ignore_patterns('.*'))

def copyOrLink(paramName, srcDir, dstDir, copy=True):
    cfg = getConfigManager()
    filesToCopy = cfg.getParam(paramName)
    fileList    = filesToCopy.split()
    _logger.debug('%sing to %s: %s' % (('Copy' if copy else 'Link'), dstDir, fileList))

    for f in fileList:
        srcPath = os.path.join(srcDir, f)
        dstPath = os.path.join(dstDir, f)

        # ensure that required parent directories exist ("file" may include non-existent parent dirs)
        mkdirs(os.path.dirname(dstPath))

        # delete destination, whether file, link, or directory
        if os.path.lexists(dstPath):
            _logger.debug("Removing %s", dstPath)
            if os.path.isdir(dstPath) and not os.path.islink(dstPath):
                rmtree(dstPath)
            else:
                os.remove(dstPath)

        if copy:
            if os.path.isdir(srcPath):
                _logger.debug("Copying %s to %s", srcPath, dstPath)
                # symlinks=True means copy symlinks as symlinks rather than copying through them
                copytree(srcPath, dstPath, symlinks=True, ignore=ignore_patterns('.*'))
            else:
                filecopy(srcPath, dstPath)  # writes debug message, so no need for one here
        else:
            symlink(srcPath, dstPath)       # writes debug message

def getBatchQueryList(queryFile, basenameOnly=False):
    '''
    Return a list query names from the given query file, stripping off
    leading and trailing whitespace, ignoring blank lines and comments.
    If stripExt is True, remove all filename extensions.
    '''
    try:
        with open(queryFile, 'r') as f:
            queryList = f.readlines()
    except Exception as e:
        raise CoreMcsUserError("Can't open batchQueryFile '%s': %s" % (queryFile, e))

    stripped = map(str.strip, queryList)
    filtered = filter(lambda s: s and s[0] != '#', stripped)

    if basenameOnly:
        filtered = map(lambda fname: os.path.splitext(os.path.basename(fname))[0], filtered)

    return filtered


class GcamConfiguration(CoreConfiguration):
    '''
    Adds gcammcs-specific configuration defaults to main configuration parser.
    '''
    def __init__(self):
        super(GcamConfiguration, self).__init__()      # Read parent's local config before ours
        self.readPackageDefaults(__file__)             # Read defaults associated with coremcs


    _activeYearStrs = None
    _activeYearInts = None

    def activeYears(self, asInt=False):
        '''
        Convert a string identifying active years into a list of ints or strs. Values must
        be comma-separated integers or expressions of the form xxxx-yyyy or xxxx-yyyy:z. The
        two expressions indicate ranges of years, with a default timestep of 5 years. If
        given, the final value after the colon indicates an alternative timestep.
        '''
        # return cached values or compute and cache result
        if not self._activeYearStrs:
            import re

            def reducer(lst, item):
                m = re.match(r"^(\d{4})-(\d{4})(?::(\d+))?$", item)
                if m:
                    start = int(m.group(1))
                    end   = int(m.group(2))
                    step  = int(m.group(3)) if m.lastindex == 3 else 5    # default 5 yr timestep
                    rng = range(start, end+1, step)
                    lst.extend(rng)
                elif item.isdigit():
                    lst.append(int(item))
                else:
                    raise CoreMcsUserError('Element in list of active years is not an integer: %s' % item)

                return lst

            yearStr = self.getParam('GCAM.Years')
            items = yearStr.split(',')
            years = reduce(reducer, items, [])

            self._activeYearInts = map(int, years)
            self._activeYearStrs = map(str, years)

        return self._activeYearInts if asInt else self._activeYearStrs


class GcamPackage(CorePackage):
    '''
    This class customizes the CorePackage to use the required classes for the
    package-specific config manager and database.
    '''
    def __init__(self):
        from coremcs.Database import setDatabaseClass   # lazy import

        getConfigManager(cls=GcamConfiguration)         # use the local config class
        setDatabaseClass(GcamDatabase)                  # use the local database class
        super(GcamPackage, self).__init__()

        self.extendCommands()
        self.createCommands()

        self.paramFile = None
        self.scenarioFile = None

    def createCommands(self):
        self.addCmd_Diff()
        self.addCmd_addOutputs()
        self.addCmd_copyXml()
        # self.addCmd_Scenario()

    def extendCommands(self):
        '''
        Extend the 'new' and 'queue' subcommands with additional arguments
        '''
        cfg = self.config

        #
        # NEW
        #
        parser = self.get_subparser(NEWAPP_CMD)

        workspaceDir = cfg.getParam('GCAM.Workspace')
        parser.add_argument('-g', '--gcamDir', default=workspaceDir,
                            help='''Use the given GCAM directory as the source of run-time files.
                            Defaults to value of config var GCAM.Workspace (currently %s)''' % workspaceDir)

        queryDir = cfg.getParam('GCAM.UserQueryDir')
        parser.add_argument('-q', '--queryDir',  default=queryDir,
                            help='''Identify the directory containing result query files.
                            Defaults to value of config var GCAM.UserQueryDir (currently %s)''' % queryDir)
        #
        # GENSIM
        #
        parser = self.get_subparser(GENSIM_CMD)

        parser.add_argument('-S', '--scenarioDir', default=None,
                            help='''Identify the directory containing scenario files.
                            Defaults to value of config var GCAM.UserScenarioDir''')
        #
        # QUEUE
        #
        parser = self.get_subparser(QUEUE_CMD)

        parser.add_argument('-b', '--batchQueryFile', type=str, default=None,
                            help='''Specify a batch query file. Defaults to value of config var GCAM.BatchQueryFile''')

        parser.add_argument('-B', '--noBatchQueries', action='store_true',
                            help='Skip running batch queries.')

        parser.add_argument('-D', '--noDatabase', action='store_true',
                            help='''Don't save query results to the SQL database.''')

        parser.add_argument('-G', '--noGCAM', action="store_true",
                            help="Don't run GCAM, just run the batch queries and post-processor (if defined).")

        parser.add_argument('-p', '--postProcessor', type=str, default='',
                            help='''Specify the path to a script to run after GCAM completes. It should accept three
                                    command-line arguments, the first being the path to the workspace in which GCAM
                                    was executed, the second being the name of the configuration file used, and the
                                    third being the scenario name of interest. Defaults to value of configuration
                                    parameter GCAM.PostProcessor.''')

        parser.add_argument('-P', '--noPostProcessor', action='store_true', default=False,
                            help='''Don't run the post-processor script. (Use this to skip calling the
                            post-processing script named in the configuration file.)''')

        #
        # Analyze
        #
        # TBD: integrate plotting logic and cmdline args from chartGCAM.py
        parser = self.get_subparser(ANALYZE_CMD)
        parser.add_argument('-t', '--timeseries', action='store_true',
                            help='Plot a timeseries distribution')

        parser.add_argument('-R', '--regionName', default=None,
                            help='The region to plot timeseries results for')

        parser.add_argument('--ymax', type=float, default=None,
                            help='''Set the scale of a figure by indicating the value to show as the
                            maximum Y value. (By default, scale is set according to the data.)''')

        parser.add_argument('--ymin', type=float, default=None,
                            help='''Set the scale of a figure by indicating the value (given as abs(value),
                            but used as -value) to show as the minimum Y value''')

    def addCmd_addOutputs(self):
        parser = self.add_subparser(ADD_OUTPUTS_CMD,
                                    help='''Update the output table of the SQL database to include all definitions
                                    from the XML file that is the value of config parameter GCAM.UserResultFile''')
        parser.set_defaults(_func=self.addOutputsCmd)
        # No arguments thus far...
        return parser

    def addCmd_Diff(self):
        parser = self.add_subparser(DIFF_CMD,
                                    help='''Compute the differences between the batch query results for two scenarios
                                            by calling the csvDiff.py script on the respective .csv files and save
                                            the results in the SQL database, as indicated by results.xml.''')
        parser.set_defaults(_func=self.diffCmd)

        parser.add_argument('-b', '--baseline', type=str,
                            help='''The name of the "baseline" scenario to use for the comparison. If not specified,
                                    the scenario from which the policy scenario is derived, as defined in the
                                    scenario file (see -S option, below).''')

        parser.add_argument('-d', '--delay', type=float, default=None,
                            help='''Insert a delay of the given number of seconds between saves to avoid NFS locking problems.''')

        parser.add_argument('-D', '--noDatabase', action='store_true',
                            help='''Don't save query results to the SQL database.''')

        parser.add_argument('-p', '--policy', type=str, default=None, required=True,
                            help='''The name of the "policy" scenario to use for the comparison.''')

        parser.add_argument('-q', '--batchQueryFile', type=str, default=None,
                            help='''A file containing the list of queries for which to perform the difference operation.
                                    (Default from config parameter GCAM.BatchQueryFile)''')

        parser.add_argument('-Q', '--noQueries', action='store_true',
                            help='''Don't run any queries (e.g., just save results to the database)''')

        parser.add_argument('-s', '--simId', type=int, default=None,
                            help='The id of the simulation to operate on.')

        parser.add_argument('-t', '--trials', type=str, default=None,
                             help='''Comma separated list of trial or ranges of trials for which to compute differences.
                                     Ex: 1,4,6-10,3. If not specified, differences are computed for all trials.''')

        parser.add_argument('-y', '--years', type=str, default=None,
                        help='''Takes a parameter of the form XXXX-YYYY, indicating start and end years of interest.
                                Other years are dropped. Default is to use all years available in the query results.''')
        return parser

    def addCmd_copyXml(self):
        parser = self.add_subparser(COPY_XML_CMD, help='Copy XML setup files to MCS "run" space.')
        parser.set_defaults(_func=self.copyXmlCmd)

        parser.add_argument('-s', '--simId', type=int, default=None, required=True,
                            help='The id of the simulation to operate on.')

        parser.add_argument('-S', '--scenarioDir', default=None,
                            help='''Identify the directory containing scenario files. Defaults to
                            value of config var GCAM.UserScenarioDir''')
        return parser


    def _addFileArguments(self, parser):
        '''
        Override the default arguments for param and distro file, creating one flag for parameters.xml.

        Comment from superclass:
        Split out from addCmd_GenSim to allow subclasses to use different
        parameter and distro file conventions.
        :param parser: the subparser for the 'gensim' command
        :return: nothing
        '''
        parser.add_argument('-p', '--param',  dest='paramFile', type=str, default=None,
                            help='''Specify an XML file containing parameter definitions. (Defaults
                            to the value of config parameter GCAM.UserParameterFile''')

    # Deprecated -- eliminate config file parameters, too
    def createTrialWorkspace(self, trialDir, runWorkspace):
        # '''
        # Called after parameters.xml has been created in the trialDir, to
        # create links to this application's copies of required GCAM files
        # and directories.
        # '''
        # _logger.debug("Creating workspace in %s", trialDir)
        #
        # cfg = getConfigManager()
        # dirsToMake  = cfg.getParam('GCAM.TrialDirsToMake').split()
        # filesToLink = cfg.getParam('GCAM.TrialFilesToLink').split()
        #
        # for dir in dirsToMake:
        #     dstDir = os.path.join(trialDir, dir)
        #     mkdirs(dstDir)
        #
        # for file in filesToLink:
        #     srcPath = os.path.join(runWorkspace, file)
        #     dstPath = os.path.join(trialDir, file)
        #     relPath = os.path.relpath(srcPath, os.path.dirname(dstPath))
        #     symlink(relPath, dstPath)
        pass

    @staticmethod
    def writeTrialDataFile(simId, rvList, matrix):
        '''
        Save the trial data matrix in the file 'trialData.csv' in the simDir.
        '''
        import pandas as pd

        paramNames = map(lambda obj: obj.getParameter().getName(), rvList)
        df = pd.DataFrame(matrix, columns=paramNames)
        simDir = getSimDir(simId)
        dataFile = os.path.join(simDir, 'trialData.csv')
        df.to_csv(dataFile)

    @staticmethod
    def readTrialDataFile(simId):
        """
        Load trial data (e.g., saved by writeTrialDataFile) and return a numpy ndarray.
        """
        import pandas as pd

        simDir = getSimDir(simId)
        dataFile = os.path.join(simDir, 'trialData.csv')

        df = pd.read_table(dataFile, sep=',', index_col=None)
        return df.as_matrix()

    def genTrialData(self, trials, simId):
        """
        Generate the given number of trials for the given simId, using the objects created
        by parsing parameters.xml. Return a matrix of results (actually, a numpy ndarray).
        """
        from coremcs.LHS import lhs                          # lazy imports for faster startup
        from .XMLParameterFile import XMLRandomVar, XMLCorrelation

        rvList = XMLRandomVar.getInstances()
        corrMatrix = XMLCorrelation.corrMatrix()
        matrix = lhs(rvList, trials, corrMat=corrMatrix)

        # Save data in preparation for splitting random value generation from input file generation.
        # Useful for testing: load csv into DataFrame and verify statistics of columns match
        # the intended distributions.
        self.writeTrialDataFile(simId, rvList, matrix)
        return matrix

    def applyTrialData(self, matrix, simId, start=0):
        """
        Apply the data from matrix to simulation simId, writing the resulting
        XML files to the local trial-xml directory for this trial.
        """
        from .XMLParameterFile import XMLParameter
        trials = matrix.shape[0]

        for trial in xrange(trials):
            trialNum = trial + start
            trialDir = getTrialDir(simId, trialNum, create=True)

            XMLParameter.applyTrial(trial, matrix)      # Update all parameters as required
            self.paramFile.writeLocalXmlFiles(trialDir) # NB: creates trial-xml subdir

    def saveTrialData(self, matrix, simId, start=0):
        """
        Save the trial data in `matrix` to the SQL database, for the given simId.
        """
        from coremcs.Database import getDatabase
        from .XMLParameterFile import XMLRandomVar

        trials = matrix.shape[0]

        # Delete all Trial entries for this simId and this range of trialNums
        db = getDatabase()

        session = db.Session()
        paramValues = []

        for trial in xrange(trials):
            trialNum = trial + start

            # Save all RV values to the database
            instances = XMLRandomVar.getInstances()
            for var in instances:
                varNum  = var.getVarNum()
                value   = matrix[trial, varNum]
                param   = var.getParameter()
                pname   = param.getName()
                paramId = db.getParamId(pname)
                paramValues.append((trialNum, paramId, value, varNum))

        # Add all the Trial records
        session.commit()
        session.close()

        # Write the tuples (simId, trialNum, paramId, value) to the database
        db.saveParameterValues(simId, paramValues)

    # TBD: add this as a sub-command
    def addScenCmd(self, simId, scenNames, matrix=None):
        """
        Add scenarios to a simulation by generating any required XML files using the
        given data matrix, or if none provided, the trial data saved by gensim.
        """
        from .XMLParameterFile import XMLParameterFile

        _logger.info("addScenarios: Adding scenarios %s", scenNames)

        # Read parameters.xml, which was stashed in the simDir by genSimulation
        simDir = getSimDir(simId)

        paramPath = os.path.join(simDir, PARAM_FILENAME)
        self.paramFile = XMLParameterFile(paramPath)
        self.paramFile.loadInputFiles(simId, scenNames)

        matrix = matrix or self.readTrialDataFile(simId)
        self.applyTrialData(matrix, simId)

        _logger.info("addScenarios: Finished adding trials")


    # TBD: We copy and perturb the reference input files, not the scenario versions. So if
    # TBD: scenarios also manipulate the same files, whichever goes last "wins". Document this!
    def genSimulation(self, program, simId, trials, paramPath, distroFile, args):
        '''
        Generate a simulation based on the given parameters (generally from the command-line).
        N.B. Overrides CorePackage method (doesn't call super; we handle it completely here.)
        '''
        from .XMLParameterFile import XMLParameterFile
        from .XMLScenarioFile import XMLScenarioFile
        from .util import getSimLocalXmlDir, getSimScenarioFile, getSimParameterFile, getSimResultFile

        cfg = self.config

        #subdir = cfg.getParam('GCAM.ScenarioSubdir')    # TBD: document this, if it's kept...
        subdir = ""                                      # TBD: Copies entire local-xml directory to capture common/new-reference

        simDir = getSimDir(simId, create=True)

        # Copy user's scenario XML files to {simDir}/local-xml/{subdir, if any}
        scenarioDir = args.scenarioDir or cfg.getParam('GCAM.UserScenarioDir')
        srcSimXmlDir = os.path.join(scenarioDir, subdir)
        dstSimXmlDir = os.path.join(getSimLocalXmlDir(simId), subdir)
        copyDir(srcSimXmlDir, dstSimXmlDir)

        # Copy the user's scenarios.xml file to {simDir}/app-xml
        userScenFile = cfg.getParam('GCAM.UserScenarioFile')
        simScenarioFile = getSimScenarioFile(simId)
        dirs = os.path.dirname(simScenarioFile)
        mkdirs(dirs)
        filecopy(userScenFile, simScenarioFile)

        # Copy the user's results.xml file to {simDir}/app-xml
        userResultFile = cfg.getParam('GCAM.UserResultFile')
        simResultFile  = getSimResultFile(simId)
        filecopy(userResultFile, simResultFile)

        # Add symlink to workspace's input dir so we can find XML files using rel paths in config files
        runInputDir = cfg.getParam('GCAM.RunInputDir')
        simInputDir = os.path.join(simDir, 'input')
        symlink(runInputDir, simInputDir)

        # Read scenarios.xml for the names of defined scenarios.
        scenFile = XMLScenarioFile(simId=simId)
        scenNames = scenFile.getScenarioNames(which='active')

        if not trials:
            _logger.warn("Simulation meta-data has been copied.")
            return

        # Ensure that the experiments (scenarios) are defined
        scenFile.addExperiments()

        self.paramFile = XMLParameterFile(paramPath)
        self.paramFile.loadInputFiles(simId, scenNames)
        self.paramFile.runQueries()

        _logger.info("Generating %d trials to %r", trials, simDir)
        matrix = self.genTrialData(trials, simId)
        self.applyTrialData(matrix, simId)
        _logger.info("Finished creating trials")

        # Save generated values to the database for post-processing
        self.saveTrialData(matrix, simId)

        # Also save the param file as parameters.xml, for reference only
        simParamFile = getSimParameterFile(simId)
        filecopy(paramPath, simParamFile)

    def genSimCmd(self, args):
        '''
        Extends core gensim to save args
        '''
        simId = super(GcamPackage, self).genSimCmd(args)

        # Save a copy of the arguments used to create this simulation
        simDir  = getSimDir(simId)
        argSaveFile = '%s/gcamGenSimArgs.txt' % simDir
        saveDict(vars(args), argSaveFile)

    def copyXmlCmd(self, args):
        """
        Copy XML files but don't generate simulations or modify the database.
        """
        simId = args.simId
        simDir = getSimDir(simId, create=False)
        if not os.path.lexists(simDir):
            raise CoreMcsUserError("Can't copy XML files: directory '%s' does not exist." % simDir)

        program = None
        trials = 0
        paramPath = None
        distroFile = None

        self.genSimulation(program, simId, trials, paramPath, distroFile, args)

    def subcmdsThatDontStartDb(self):
        """
        Add the copyxml command to the list of those for which the database isn't started.
        """
        subcmds = super(GcamPackage, self).subcmdsThatDontStartDb()
        subcmds.append(COPY_XML_CMD)
        return subcmds

    def diffCmd(self, args):
        """
        Compute differences between .csv files creates by batch queries
        """
        from subprocess import check_output, STDOUT, CalledProcessError
        from coremcs.Database import getDatabase
        from .XMLResultFile import QUERY_OUTPUT_DIR, RESULT_TYPE_DIFF, saveResults

        cfg = self.config

        simId    = args.simId
        baseline = args.baseline or cfg.getParam('GCAM.BaseScenario')
        policy   = args.policy
        years    = args.years
        trialStr = args.trials
        delay    = args.delay
        batchQueryFile = args.batchQueryFile or cfg.getParam('GCAM.BatchQueryFile')
        noDatabase = args.noDatabase
        noQueries  = args.noQueries

        db = getDatabase()

        # If baseline not given, read parent from experiment table
        if not baseline:
            baseline = db.getExpParent(args.policy)
            _logger.debug("Parent of '%s' is '%s'", policy, baseline)

        diffScript = cfg.getParam('GCAM.DiffScript')

        queryList = [] if noQueries else getBatchQueryList(batchQueryFile, basenameOnly=True)

        if trialStr:
            trials = parseTrialString(trialStr)
        else:
            trialCount = db.getTrialCount(simId)
            trials = xrange(trialCount)

        def verifyFileExists(path):
            if not os.path.lexists(path):
                raise CoreMcsUserError("The file '%s' does not exist." % path)

        for trialNum in trials:
            trialDir = getTrialDir(simId=simId, trialNum=trialNum)

            baselineDir = os.path.join(trialDir, baseline, QUERY_OUTPUT_DIR)
            policyDir   = os.path.join(trialDir, policy,   QUERY_OUTPUT_DIR)
            diffsDir    = os.path.join(policyDir, 'diffs')

            mkdirs(diffsDir) # creates all parent dirs if necessary

            for queryName in queryList:
                baselineCsv = os.path.join(baselineDir, "%s-%s.csv" % (queryName, baseline))
                policyCsv   = os.path.join(policyDir,   "%s-%s.csv" % (queryName, policy))

                try:
                    verifyFileExists(baselineCsv)
                    verifyFileExists(policyCsv)
                except CoreMcsUserError as e:
                    _logger.error(e)
                    continue

                outName = '%s-%s-%s.csv' % (queryName, policy, baseline)
                outFile = os.path.join(diffsDir, outName)

                diffArgs = [diffScript, baselineCsv, policyCsv, '-o', outFile]

                if years:
                    diffArgs.extend(('-y', years))

                # TBD: this might not be an adequate heuristic...
                interpolate = not queryName.endswith('_forcing')
                if interpolate:
                    diffArgs.append('-i')

                _logger.debug("Running: %s", ' '.join(diffArgs))
                try:
                    check_output(diffArgs, shell=False, stderr=STDOUT)

                except CalledProcessError as e:
                    msg  = e.output.strip()
                    code = e.returncode
                    raise CoreMcsSystemError("%s returned status %d: %s" % (diffScript, code, msg))

            if not noDatabase:
                run = db.getRun(simId, trialNum, policy)
                try:
                    saveResults(run.runId, policy, RESULT_TYPE_DIFF, baseline=baseline, delay=delay)

                except Exception as e:
                    _logger.error("Failed to save diff result for trial %d, policy %s: %s", trialNum, policy, e)

    @staticmethod
    def stripYearPrefix(s):
        '''
        If s is of the form y + 4 digits, remove the y and convert the rest to integer
        '''
        if len(s) == 5 and s[0] == 'y':
            try:
                return int(s[1:])
            except ValueError:
                pass

        return s

    def analyzeCmd(self, args):
        if args.timeseries:
            from coremcs.Database import getDatabase
            from coremcs.timeseriesPlot import plotTimeSeries
            import pandas as pd

            simId      = args.simId
            expList    = args.expName.split(',')
            resultName = args.resultName
            xlabel     = 'Year' # args.xlabel or 'Year'
            ymin       = args.ymin
            ymax       = args.ymax

            if not (expList[0] and resultName):
                raise CoreMcsUserError("expName and resultName must be specified")

            db = getDatabase()
            trialCount = db.getTrialCount(simId)

            cfg = self.config
            plotDir  = cfg.getParam('Core.PlotDir')
            plotType = cfg.getParam('Core.PlotType')

            for expName in expList:
                results = db.getTimeSeries(simId, resultName, expName) #, regionName)
                if not results:
                    raise CoreMcsUserError('No timeseries results for simId=%d, expName=%s, resultName=%s' \
                                           % (simId, expName, resultName))

                _logger.debug("Found %d result records for exp %s, result %s" % \
                              (len(results), expName, resultName))

                # massage the data into the format required by plotTimeSeries
                records = [obj.__dict__ for obj in results]
                resultDF = pd.DataFrame.from_records(records, index='seriesId')
                units = resultDF.units.iloc[0]
                resultDF.drop(['units', '_sa_instance_state', 'outputId'], axis=1, inplace=True)

                # convert column names like 'y2020' to '2020'
                cols = map(self.stripYearPrefix, resultDF.columns)
                resultDF.columns = cols

                df = pd.melt(resultDF, id_vars=['runId'], var_name='year')

                title = '%s for %s' % (resultName, expName)
                reg = "" # '-' + regionName if regionName else ""
                basename = "%s-s%d-%s%s.%s" % (resultName, simId, expName, reg, plotType)
                filename = os.path.join(plotDir, 's%d' % simId, basename)
                _logger.debug('Saving timeseries plot to %s' % filename)

                extra = "name=%s trials=%d/%d simId=%d scenario=%s%s" % \
                        (resultName, resultDF.shape[0], trialCount, simId, expName, reg)

                # TBD: generalize this with a lookup table or file
                if units == 'W/m^2':
                    units = 'W m$^{-2}$'

                plotTimeSeries(df, 'year', 'runId', title=title, xlabel=xlabel, ylabel=units, ci=[95], # [50, 95],
                               text_label=None, # text_label='95% CI', # ['50% CI', '95% CI'],
                               legend_name=None, legend_labels=None,
                               ymin=ymin, ymax=ymax, filename=filename, show_figure=False, extra=extra)

            # if doing timeseries plot, none of the other options are relevant
            return

        super(GcamPackage, self).analyzeCmd(args)

    def addOutputsCmd(self, args):
        """
        Update result (output) definitions
        """
        from .XMLResultFile import XMLResultFile

        cfg = self.config
        resultsFile = cfg.getParam('GCAM.UserResultFile')
        rf = XMLResultFile(resultsFile)
        rf.saveOutputDefs()

    def newAppCmd(self, args):
        '''
        Extend coremcs version to copy required GCAM files to run directory.
        '''
        from .util import getRunQueryDir

        super(GcamPackage, self).newAppCmd(args)

        cfg = self.config
        dstDir = cfg.getParam('GCAM.RunWorkspace')
        srcDir = args.gcamDir
        cfg.setParam('GCAM.Workspace', srcDir)  # save this value in the user's config file

        rmlink(dstDir)      # remove old link if one exists
        mkdirs(dstDir)      # ensure that directories exist

        copyOrLink('GCAM.FilesToCopy', srcDir, dstDir, copy=True)
        copyOrLink('GCAM.FilesToLink', srcDir, dstDir, copy=False)

        runQueryDir  = getRunQueryDir()
        userQueryDir = cfg.getParam('GCAM.UserQueryDir')
        copyDir(userQueryDir, runQueryDir)

        self.addOutputsCmd(None)

    def initdbCmd(self, args):
        '''
        Extend coremcs version to add output definitions
        '''
        super(GcamPackage, self).initdbCmd(args)
        self.addOutputsCmd(None)


    @classmethod
    def getInitialData(cls):
        '''
        Return the data this package needs added to a newly initialized database.
        Value must be a list of pairs, where the first element is a table name,
        and the second is a list of table rows expressed as dictionaries. Subclasses
        get the list from super and add it to their own.
        '''
        superData = super(GcamPackage, cls).getInitialData()

        ourData = [
            ['Program',    [{'name': GCAM_PROGRAM,
                             'description' : 'The GCAM executable program'}]],
            # Deprecated?
            ['Attribute',  [{'attrName'    : 'gcamStatus',
                             'attrType'    : 'number',
                             'description' : 'exit status of GCAM program'}]]
        ]
        return superData + ourData


    @classmethod
    def getParamFile(cls, program):
        '''
        Given a program name, compute the name of the corresponding parameter file.
        '''
        if program == GCAM_PROGRAM:     # the only choice for now...
            cfg = self.config
            return cfg.getParam('GCAM.UserParameterFile')

        return super(GcamPackage, cls).getParamFile(program)


    @classmethod
    def defineSets(cls):
        # Read parent class's sets first
        super(GcamPackage, cls).defineSets()

        # Create aliases as needed to patch differences in names among files
        # createSetAliases()
