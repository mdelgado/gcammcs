'''
Created on Jul 20, 2014

@author: rjp
'''
import unittest
import pandas as pd
from coremcs.Configuration import getConfigManager, DEFAULT_SECTION, APP_NAME_OPTION
from coremcs.Distro import genDistros
from gcammcs.GCAM import QueryResult
from gcammcs.Package import GcamConfiguration
from gcammcs.Database import getDatabase, GcamDatabase
from gcammcs.XML import XMLParameterFile


# Run tests against the 'mcstest' database
APP_NAME = 'mcstest'


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cfg = getConfigManager(GcamConfiguration)
        cfg.parser.set(DEFAULT_SECTION, APP_NAME_OPTION, APP_NAME)
        genDistros()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_A_QueryResult(self):
        result = QueryResult('../xml/co2-by-sector.csv')

        df = result.getData()
        self.assertEqual(df.shape, (30, 16), 'Dataframe is not 30x16 as expected')

        queryTitle = result.getTitle()
        self.assertEqual(queryTitle, 'CO2 emissions by sector', 'Title mismatch')

        levels = result.firstNumericCol()
        self.assertEqual(levels, 3, 'Wrong number of columns (levels) preceding numeric column name')


    def test_B_StoreResult(self):
        cfg    = getConfigManager()
        result = QueryResult('../xml/co2-by-sector.csv')
        df     = result.getData()
        title  = result.getTitle()
        years  = cfg.activeYears()
        sums   = pd.DataFrame(df[years].sum()).transpose()

        print sums
        # e.g.,
        #          2005         2010         2015         2020         2025
        # 0  1616.57341  1676.100559  1732.313598  1766.002646  1809.406477

        db = getDatabase(GcamDatabase)
        db.initDb()     # drop and recreate all tables to start in a known state

        yearCols = db.yearCols()    # adds "y" prefix to numeric years

#         db.execute('delete from series;')
#         db.execute('delete from run')
#         db.execute('delete from sim')
#         db.execute('delete from experiment')
#         db.execute('delete from output')

        # Set up data to satisfy foreign keys
        db.execute('''insert into experiment ("expId", "expName", description) values (1, 'testExp', 'just for testing')''')
        db.execute('''insert into sim ("simId", trials, description) values (1, 1000, 'just for testing')''')
        db.execute('''insert into output("outputId", name, description) values (1, '%s', 'test output')''' % title)
        db.execute('''insert into run("runId", "simId", "expId", "trialNum") values (1, 1, 1, 1)''')

        sql = '''insert into series ("runId","outputId",%s) values (1,1,%s)''' % \
            (','.join(yearCols), ','.join(map(str, sums.values[0])))
        db.execute(sql)

    # def test_C_GCAM(self):
    #     simId    = 2
    #     trialNum = 0
    #     expName  = 'baseline'
    #     trials   = 1
    #
    #     gcam = GCAM.getInstance()
    #     if False:
    #         gcam.createTrialWorkspace(simId, trialNum)
    #
    #     paramFile = XMLParameterFile('testFiles/parameters.xml')
    #     paramFile.runQueries()
    #     gcam.genTrials(trials, expName, simId, start=0)

#     if  False:
#         _title, data = readCSV('/Users/rjp/Projects/Software/GCAM/current/Main_User_Workspace/output/core_output.csv')
#         header = data[0]
#         data.pop(0)
#         rows = len(data)
#         cols = len(header)
#
#         # count the non-numeric values prior to the first numeric value
#         firstrow = data[0]
#         levels   = 0
#         for col in range(cols):
#             if is_number(firstrow[col]):
#                 levels = col
#                 break
#
#         print "%d levels" % levels
#         hier = dict()
#         for row in range(rows):
#             d = hier
#             datarow = data[row]
#             for col in range(levels): #-1):
#                 name = datarow[col]
#                 if not d.has_key(name):
#                     d[name] = dict()
#
#                 d = d[name]
#
#             # leaf of hierarchy is a vector of values
#             vector = d[datarow[levels-1]] = []
#
#             for col in range(levels, cols):
#                 vector.append(datarow[col])
#
#         printHier(hier, header)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
