'''
Created on Jul 21, 2014

@author: rjp
'''
import unittest
from coremcs.Database import getDatabase
from gcammcs.Database import GcamDatabase
from gcammcs.Package import getConfigManager, GcamConfiguration


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testName(self):
        pass

    # TODO: make this do something
    def tessMain(self):
        _cfg = getConfigManager(GcamConfiguration)     # use the local config and database classes
        getDatabase(GcamDatabase)

    # TODO: decide if worth fixing. Is XMLDatabase being used?
    # def XXtestOther(self):
    #     simId    = 1
    #     trialNum = 1
    #     expName  = 'base'
    #     # appName  = 'testApp'
    #     expDir   = getExpDir(simId, trialNum, expName)
    #     outputDir = os.path.join(expDir, 'output')
    #     os.chdir(outputDir)
    #
    #     containerName = "database.dbxml"
    #     xmldb = XMLDatabase(containerName)
    #
    #     # qry = '''*[@type = 'sector' and
    #     #             ((@name='H2 Central Production' or
    #     #               @name='H2 Forecourt Production' or
    #     #               @name='H2 Enduse' or
    #     #               @name='H2_td'))]/cost/text()'''
    #     #qry = '''//*[@type='sector' and (@name='H2 Central Production' or @name='H2 Forecourt Production')]/*[@type='subsector']/cost/text()'''
    #     #qry = '''//LandNode[@name='root' or @type='LandNode' (: collapse :)]//land-use-change-emission/text()'''
    #     qry = '''//LandNode[@name='root' or @type='LandNode' (: collapse :)]//land-use-change-emission[@year>2000]/text()'''
    #     #qry = "/scenario/output-meta-data/primary-fuel"
    #     result = xmldb.query(qry)
    #
    #     print "%d result(s)" % result.size()
    #
    #     # Print results. XmlResult is an iterator
    #     total = 0
    #     for value in result:
    #         print "Result: %s" % value.asString()
    #         total += value.asNumber()
    #     print "Total: ", total


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
