'''
@author Joel Bremson
@author Richard Plevin
'''

import unittest
import os

from lxml import etree as ET

from coremcs.Distro import genDistros
from coremcs.Configuration import getConfigManager
from gcammcs.Package import GcamConfiguration
from gcammcs.XML import XMLParameterFile, XMLFile, XMLDataFile

DATADIR = 'testFiles'

class XMLTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        getConfigManager(GcamConfiguration)
        genDistros()

    def testFile(self):
        filename = os.path.join(DATADIR, 'test.xml')
        xmlfile = XMLFile(filename, load=True)
        root = xmlfile.getRoot()
        self.assertEqual(len(root), 1)
        world = root[0]
        self.assertEqual(len(world), 4)

        region = world[0]
        self.assertEqual(region.get('name'), 'Africa')

    # def testParamFile(self):
    #     filename = os.path.join(DATADIR, 'parameters.xml')
    #     xmlParamFile = XMLParameterFile(filename)
    #     self.assertEqual(['co2-conc'], xmlParamFile.getOutputs())

    # TBD: fix this... it's now <Parameter name="xxx"><Exogenous file="yyy" /></Parameter>

    def testExogenousData(self):
        filename = os.path.join(DATADIR, 'exogenous.txt')
        data = XMLDataFile.getData(filename)
        self.assertEqual(data['baz'][1], 22)

    def testCopySubtree(self):
        filename = os.path.join(DATADIR, 'copytest.xml')
        xmlfile  = XMLFile(filename)
        tree     = xmlfile.getTree()
        xpath    = '//scenario/world/region/technology[@name="foo"]/cost'
        elements = tree.xpath(xpath)
        subtree  = Subtree()
        _copies  = subtree.copyElements(elements)
        root     = subtree.getRoot()
        expected = \
'''<scenario>
  <world>
    <region name="Africa">
      <technology name="foo">
        <cost>10.2</cost>
      </technology>
    </region>
    <region name="USA">
      <technology name="foo">
        <cost>131.4</cost>
      </technology>
    </region>
    <region name="India">
      <technology name="foo">
        <cost>6.3</cost>
      </technology>
    </region>
  </world>
</scenario>
'''
        self.assertEqual(ET.tostring(root, pretty_print=True), expected)


#         xml = XML()
#         xml.run()
#         #keys = xml.dict_keys()
#         cfg_files = xml.get_config_files()
#         self.assertEqual(len(cfg_files.keys()),4)
#         self.assertNotEqual(BASE_DIR,False)
#         self.assertEqual(len(xml.xml_files),2)
#         #vals = []
#         for xml_file in xml.get_xml_files():
#             for key,param in xml_file.get_params():
#                 # this will populate the last one
#                 pass
#         self.assertEqual(key,"bar_cost")
#         self.assertEqual(param.xpath,'//region/technology/[@name="bar"]/cost')
#         self.assertEqual(param.factor,False)



#     def test_XMLFile(self):
#         regions = 0
#         xf = XMLFile(path.join(GCAM_INPUT_DIR, "first.xml"))
#         #self.assertEqual(xf.is_factor(),True)
#         for tag in xf.tree.getroot().iter():
# #            print "\ntag - " + tag.tag
# #            print "text - " + tag.text.strip()
# #            print "attrib - " + str(tag.attrib)
#
#             # TODO: shouldn't len(x) work since it's iterable?
#             # (underscore indicates unused var in PyDev)
#             for _val in tag.iterfind('region'):
#                 #print etree.tostring(val)
#                 regions += 1
#
#         self.assertEqual(regions,3)
#         xps = XMLParameter('foo')
#         xps.add(Mock(tag='xpath',text="//foo"))
#         xps.add(Mock(tag='distribution', attrib={'type':'normal','factor':True}))
#         xf.add_param(xps)
#
#         for k,p in xf.get_params():
#             self.assertEqual(k,"foo")
#             self.assertEqual(p.xpath,"//foo")
#             self.assertEqual(p.factor,True)


#     def test_XMLFileContainer(self):
#         xfc = XMLFileContainer()
#         for i in xrange(2):
#             xfc.append(i)
#         xfc.append(2)
#         #print "Testing iterator fxn of XMLFileContainer by iterating it multiple times."
#         for i in xrange(3):
#             out = []
#             for x in xfc:
#                 out.append(x)
#             self.assertEqual(out,[0,1,2])

