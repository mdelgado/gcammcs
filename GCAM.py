'''
Created on Sep 20, 2013

@author: Richard Plevin

Copyright (c) 2014-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import os
import time
import shlex
from shutil import rmtree
from subprocess import call

import coremcs.log
from coremcs.error import TimeoutError, AlarmError, CoreMcsUserError, CoreMcsSystemError
from coremcs.Database import getDatabase, RUN_SUCCEEDED
from coremcs.util import (getContext, getExpDirFromContext, jobTmpDir, getJobNum, loadRuntimeArgs,
                          mkdirs, symlink, rmlink, getSimDir, getTrialDir, getExpDir)
from coremcs.Configuration import getConfigManager
from coremcs.Constants import RUNNER_SUCCESS, RUNNER_FAILURE
from .Package import GcamConfiguration, getBatchQueryList
from .Database import GcamDatabase
from .batchQuery import batchQuery
from .XMLResultFile import saveResults, RESULT_TYPE_SCENARIO, QUERY_OUTPUT_DIR
from .XMLScenarioFile import XMLScenarioFile
from .util import getSimScenarioFile, getSimConfigFile

_logger = coremcs.log.getLogger(__name__)

class GcamMcsGeneratorError(CoreMcsSystemError):
    '''Failed to run user-specified generator'''
    pass


def runBatchQueries(batchQueryFile, context):
    cfg = getConfigManager()
    queryFile = batchQueryFile or cfg.getParam('GCAM.BatchQueryFile')
    deleteTmpFiles = cfg.getParamAsBoolean('GCAM.DeleteBatchTmpFiles')

    if not queryFile:
        _logger.info('No file given by GCAM.BatchQueryFile; no batch queries to run.')
        return

    queryList = getBatchQueryList(queryFile)

    scenario = context.expName

    start = time.time()
    _logger.info("runBatchQueries: running queries from %s", queryFile)

    # pathnames are interpreted relative to 'exe'
    batchQuery(queryList, scenario=scenario, regions=None, workspace='..',
               outputDir=os.path.join('..', QUERY_OUTPUT_DIR),
               deleteTmpFiles=deleteTmpFiles)

    stop = time.time()
    _logger.info("runBatchQueries: elapsed time: %s", secondsToStr(stop - start))


# Help text from queueGCAM.py:
# Specify the path to a script to run after GCAM completes. It should accept three
# command-line arguments, the first being the path to the workspace in which GCAM
# was executed, the second being the name of the configuration file used, and the
# third being the scenario name of interest. Defaults to value of configuration
# parameter GCAM.PostProcessor.

def runPostProcessor(postProc, context):
    cfg = getConfigManager()
    postProc = postProc or cfg.getParam('GCAM.PostProcessor')

    if not postProc:
        _logger.info('Skipping post-processing: GCAM.PostProcessor is undefined')
        return

    # Feature: consider whether additional arguments would be useful
    argDict = {'simId'    : str(context.simId),
               'trialNum' : str(context.trialNum),
               'scenario' : context.expName,
               'appName'  : context.appName,
               'workspace': getExpDirFromContext(context=context)}

    shellCmd = postProc.format(**argDict)
    args = shlex.split(shellCmd)
    _logger.info("Running post-processor: %s", args)

    start = time.time()
    status = call(args, shell=False)
    stop = time.time()

    _logger.info("runPostProcessor: program exited with status %d. Elapsed time: %s",
                 status, secondsToStr(stop - start))


def printHier(hier, header, indent=0):
    '''
    Print a hierarchy of dictionaries holding a series of numeric values
    '''
    def tryFloat(string):
        try:
            return float(string)
        except ValueError:
            return string

    for key, val in hier.iteritems():
        print "  " * indent,
        if type(val) == dict:
            print "%s '%s':" % (header[0], key)
            printHier(val, header[1:], indent+2)
        else:
            print "%s: %s" % (header[0], map(tryFloat,val))


def workspaceSymlink(basename, dst=None, relative=True):
    cfg = getConfigManager()
    runWorkspace = cfg.getParam('GCAM.RunWorkspace')

    src = basename if os.path.isabs(basename) else os.path.join(runWorkspace, basename)

    if relative:
        src = os.path.relpath(src)    # convert to relative path

    if dst is None:
        dst = os.path.basename(src)

    try:
        os.unlink(dst)
    except OSError, e:
        if e.errno != 2:     # Ignore if "no such file or directory"
            raise

    os.symlink(src, dst)


def makeSubdirAndLinks(subdir, files):
    mkdirs(subdir)
    os.chdir(subdir)
    for f in files:
        workspaceSymlink(os.path.join(subdir, f))
    os.chdir('..')


def localSymlink(src, dst=None):
    if not dst:
        dst = os.path.basename(src)
    rmlink(dst)
    os.symlink(src, dst)


def createEpilogueLink(target):
    '''
    Create a symlink that points to the output dir associated with this job,
    which is used by the epilogue.py script to clean up. Created in user's
    home dir to avoid leaving cruft on compute node /tmp dirs.
    '''
    tmpDir = os.path.expanduser('~/.mcs/tmp')
    mkdirs(tmpDir)
    jobNum = getJobNum()
    linkName = '%s/mcs.%s.link' % (tmpDir, jobNum)
    symlink(target, linkName)
    return linkName


def setupWorkspace(gcamProgram):
    # Create a temporary directory under the TmpDir (typically, "/tmp")
    # and make a symlink called "output" that refers to it. GCAM output
    # will be created in this temporary dir, which is removed by the
    # epilogue.py script after the main job terminates. This allows GCAM
    # to use the faster "/tmp" (or on evergreen, "/dev/shm") memory-mapped
    # filesystem and guarantee that the files are removed after the run.
    dirPath = jobTmpDir()
    rmtree(dirPath, ignore_errors=True)     # rm any files from prior run in this job
    mkdirs(dirPath)
    _logger.debug("Creating 'output' link to %s" % dirPath)
    workspaceSymlink(dirPath, 'output', relative=False)

    # Create a link that the epilogue.py script can find.
    cfg = getConfigManager()
    if cfg.getParam('Core.Epilogue'):
        createEpilogueLink(dirPath)

    # Create links to this app's workspace dirs
    workspaceSymlink('libs')
    workspaceSymlink('input')

    # Create link to our scenario's copy of local-xml
    symlink('../../../local-xml', 'local-xml')

    # ... and to trial's copy of dyn-xml
    symlink('../dyn-xml', 'dyn-xml')

    # TBD: either make WriteLocalBaseXDB a value of a variable, or change the config
    # TBD: variable that defines what to copy since it's not optional or changeable.
    makeSubdirAndLinks('exe', [gcamProgram, 'log_conf.xml', 'WriteLocalBaseXDB.class'])


def secondsToStr(t):
    minutes, seconds = divmod(t, 60)
    hours, minutes   = divmod(minutes, 60)
    return "%d:%02d:%02d" % (hours, minutes, seconds)


def runGenerator(context):
    """
    Run a scenario generator command, if defined for the current scenario. This
    is called after os.chdir(exeDir). The cmd string is used as a template for
    "format", substituting simId, simDir, trialNum, trialDir, expName, expDir,
    policy (alias for expName), and baseline, where enclosed in curly braces.
    """
    expName  = context.expName
    simId    = context.simId
    trialNum = context.trialNum

    # Read scenario information to see if we need to run a generator script
    path = getSimScenarioFile(simId)
    scenFile = XMLScenarioFile(pathname=path, simId=simId)
    genCmdTemplate = scenFile.getGenerator(expName)

    if genCmdTemplate is None:
        return

    baseline = scenFile.getParentName(expName)

    # Generators require successful baseline run
    db = getDatabase()
    run = db.getRun(simId, trialNum, baseline)
    if run.status != RUN_SUCCEEDED:
        raise GcamMcsGeneratorError("Trial %d baseline (%s) status is %s; can't run %s" %
                                    (trialNum, baseline, run.status, expName))

    simDir   = getSimDir(simId)
    trialNum = context.trialNum
    trialDir = getTrialDir(simId, trialNum)
    expDir   = getExpDir(simId, trialNum, expName)
    genCmd   = genCmdTemplate.format(simId=simId, simDir=simDir,
                                     trialNum=trialNum, trialDir=trialDir,
                                     expName=expName, expDir=expDir, policy=expName,
                                     baseline=baseline, fromMCS=True)
    args = shlex.split(genCmd)
    prog = args[0]

    if not os.path.isabs(prog):
        cfg = getConfigManager()
        # TBD: generators should be copied to simdir and used from there
        genDir = cfg.getParam('GCAM.UserGeneratorDir')
        prog = os.path.abspath(os.path.join(genDir, prog))
        args[0] = prog

    try:
        cmdStr = ' '.join(args)
        _logger.debug("Calling generator: %s", cmdStr)

        status = call(args, shell=False)
        if status != 0:
            raise GcamMcsGeneratorError('Generator "%s" returned status %d' % (prog, status))

    except CoreMcsUserError:
        raise

    except Exception as e:
        raise CoreMcsUserError('Failed to run generator "%s": %s' % (prog, e))


def runGCAM(_args):
    '''
    Run GCAM in the current working directory and return exit status.
    Called from runGCAM.py on the compute node.
    '''
    _logger.info("runGCAM: entering")

    db = getDatabase(GcamDatabase)
    context = getContext()

    run = db.getRunFromContext(context)
    context.runId = run.runId

    # Load runtime args from json file in saved to expDir by QUEUE cmd
    args = loadRuntimeArgs(context=context, asNamespace=True)
    _logger.debug("args: %s", args)

    cfg = getConfigManager(GcamConfiguration)
    gcamProgram = cfg.getParam('GCAM.ProgramName')
    gcamPath = os.path.join('.', gcamProgram)

    configFile = getSimConfigFile(context.simId, args.expName)

    if args.noGCAM:
        if cfg.getParam('Core.Epilogue'):
            outputDir = os.path.realpath('./output')
            createEpilogueLink(outputDir)
    else:
        # parentScenario = db.getExpParent(args.expName)
        setupWorkspace(gcamProgram)

    javaArgs = cfg.getParam('GCAM.JavaArgs')
    if javaArgs:
        os.environ['_JAVA_OPTIONS'] = javaArgs      # required on some systems to have adequate heap space

    try:
        os.chdir('exe')

        if args.noGCAM:
            _logger.info('runGCAM: skipping GCAM')
            gcamStatus = 0

        else:
            runGenerator(context)      # run a scenario generator if one is defined for this scenario

            gcamArgs = [gcamPath, '-C%s' % configFile]      # N.B. gcam.exe doesn't allow space between -C and filename
            cmd = " ".join(gcamArgs)
            _logger.info("Running %r in %s", cmd, os.getcwd())

            start = time.time()
            gcamStatus = call(gcamArgs, shell=False)
            stop = time.time()

            _logger.info("runGCAM: GCAM exited with status %d" % gcamStatus)
            _logger.info("runGCAM: elapsed time: %s", secondsToStr(stop - start))

            filesToDelete = cfg.getParam('GCAM.FilesToDelete')
            if filesToDelete:
                files = filesToDelete.split(' ')
                for filename in files:
                    try:
                        os.remove(filename)
                    except:
                        pass    # ignore missing files

        if gcamStatus == 0:
            if not args.noBatchQueries:
                runBatchQueries(args.batchQueryFile, context)

            if not args.noPostProcessor:
                runPostProcessor(args.postProcessor, context)   # e.g., compute diffs

            if not args.noDatabase:
                saveResults(context.runId, context.expName, RESULT_TYPE_SCENARIO)

            status = RUNNER_SUCCESS
        else:
            status = RUNNER_FAILURE

    except (TimeoutError, AlarmError):
        # handled by Runner.py
        raise

    except GcamMcsGeneratorError as e:
        status = RUNNER_FAILURE
        _logger.error("runGCAM: %s" % e)

    except Exception, e:
        _logger.error("runGCAM: %s" % e)
        raise

    _logger.info("runGCAM: exiting")
    return status


if __name__ == '__main__':
    from .Package import GcamPackage
    pkg = GcamPackage()
    coremcs.log.configure(pkg.config)
