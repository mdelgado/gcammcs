'''
Created on July 5, 2013

@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.
'''

# Useful doc: http://infohost.nmt.edu/tcc/help/pubs/pylxml/web/index.html

import os
from math import ceil
from collections import OrderedDict, defaultdict

import pandas as pd
import numpy as np
from lxml import etree as ET

import coremcs.log
from coremcs.error import CoreMcsUserError, CoreMcsSystemError
from coremcs.util import mkdirs, importFromDotSpec
from coremcs.Database import getDatabase
from coremcs.Distro import DistributionSpecError, DistroGen

from .XML import XMLFile, XMLWrapper, findAndSave
from .XMLConfigFile import XMLConfigFile

_logger = coremcs.log.getLogger(__name__)


# XML parameter file element tags
INFILE_ELT_NAME      = 'InputFile'
PARAMLIST_ELT_NAME   = 'ParameterList'
PARAM_ELT_NAME       = 'Parameter'
QUERY_ELT_NAME       = 'Query'
CORRELATION_ELT_NAME = 'Correlation'
DISTRO_ELT_NAME      = 'Distribution'
DATAFILE_ELT         = 'DataFile'
PYTHON_FUNC_ELT      = 'PythonFunc'

# These attributes of a <Distribution> element are not parameters to the
# random variable distribution itself, so we exclude these from the
# list we pass when creating the RV.
DISTRO_META_ATTRS     = ['name', 'type', 'apply']
DISTRO_MODIF_ATTRS    = ['lowbound', 'highbound', 'updatezero']

#
# Define the DTD for the ParameterList so we can validate it all at once and
# avoid error checking while building out the data structure. Obviously, this
# must be updated if the structure of the ParameterList and its children changes.
# Useful for debugging errors in DTD: http://www.validome.org/grammar/
#
PARAMETER_DTD = '''
<!ELEMENT ParameterList (InputFile+)>

<!ELEMENT InputFile (Parameter+)>
<!ATTLIST InputFile
  name     CDATA #REQUIRED>

<!ELEMENT Parameter ((Query,(Distribution|DataFile), Correlation*) |
                     ((Distribution|DataFile),Query)|
                     PythonFunc)>
<!ATTLIST Parameter
  name   CDATA #REQUIRED
  mode (shared|independent|ind) "shared">

<!ELEMENT Distribution EMPTY>
<!ATTLIST Distribution
  name   CDATA #REQUIRED
  apply  (add|mult|multiply|dir|direct) "direct"
  range  CDATA #IMPLIED
  count  CDATA #IMPLIED
  min    CDATA #IMPLIED
  max    CDATA #IMPLIED
  mean   CDATA #IMPLIED
  mode   CDATA #IMPLIED
  std    CDATA #IMPLIED
  stdev  CDATA #IMPLIED
  factor CDATA #IMPLIED
  low95  CDATA #IMPLIED
  high95 CDATA #IMPLIED
  lowbound   CDATA #IMPLIED
  highbound  CDATA #IMPLIED
  updatezero CDATA #IMPLIED>

<!ELEMENT DataFile (#PCDATA)>
<!ATTLIST DataFile
  apply (add|mult|multiply|dir|direct) "direct">

<!ELEMENT Query      (#PCDATA)>
<!ELEMENT PythonFunc (#PCDATA)>

<!ELEMENT Correlation  (#PCDATA)>
<!ATTLIST Correlation
  with CDATA #REQUIRED>
'''

class XMLCorrelation(XMLWrapper):
    '''
    Store information from a <Correlate> node
    '''
    instances = []

    @classmethod
    def saveInstance(cls, obj):
        cls.instances.append(obj)


    def __init__(self, element, param):
        super(XMLCorrelation, self).__init__(element)
        self.param = param
        self.paramName = self.param.getName()
        self.otherName = element.get('with')

        try:
            self.value = float(element.text)

        except ValueError:
            raise CoreMcsUserError("In parameter %s, the value of a Correlation element (%s) is not numeric"
                                   % (self.paramName, element.text))

        _logger.debug('Defined correlation of %.3f between %s and %s',
                       self.value, self.paramName, self.otherName)

        # Save correlation instances; we process them just before generating trial data
        XMLCorrelation.saveInstance(self)

    @classmethod
    def corrMatrix(cls):
        '''
        Generate a correlation matrix representing the correlation definitions,
        or None if no Correlation definitions were found.
        '''
        if not cls.instances:
            return None

        params = XMLParameter.getInstances()
        count  = len(params)

        corrMat = np.zeros((count, count), dtype=float)

        for i in xrange(count):
            corrMat[i][i] = 1.0  # diagonal must be 1

        for corr in cls.instances:
            p1 = corr.param
            p2 = corr.other
            coef = corr.value

            src1 = p1.getDataSrc()
            src2 = p2.getDataSrc()

            # TBD: Restrict correlation to Distribution, or ok with PythonFunc or DataFile?

            if src1.isShared() != src2.isShared():
                raise CoreMcsUserError('''
    Parameters '%s' and '%s' cannot be correlated because
    one is 'shared' and the other is 'independent'.''' % (corr.paramName, corr.otherName))

            if src1.isShared():
                varNum1 = p1.rv.getVarNum()
                varNum2 = p2.rv.getVarNum()

                if varNum1 == varNum2:
                    raise CoreMcsUserError('''
    Parameter '%s' has a single (shared) random variable; it cannot be self-correlated.''' % corr.paramName)
                corrMat[varNum1][varNum2] = corrMat[varNum2][varNum1] = coef

            else:
                # If the parameters have independent RVs per XML element, two parameters
                # can be correlated if they have the same number of RVs, and the RVs of
                # a single param can be correlated pairwise amongst themselves.
                if p1 == p2:
                    raise CoreMcsUserError('''
    Parameter '%s' has %d independent random variables; it cannot be self-correlated.''' % (corr.paramName, len(p1.getVars())))
                    # Create pairwise correlations among the RVs. This is probably a bad idea if
                    # the query returns more than a few dozen elements, as is usually the case.
                    # vars = p1.getVars()
                    #
                    # from itertools import permutations
                    # # create all unique pairwise combinations of parameters
                    # # and then map these to pairwise combinations of var nums
                    # varNumPairs = map(lambda pair: (pair[0].rv.getVarNum(),
                    #                                 pair[1].rv.getVarNum()),
                    #                   permutations(vars))
                else:
                    # Two different parameters with independent RVs.
                    p1Vars = p1.getVars()
                    p2Vars = p2.getVars()

                    if len(p1Vars) != len(p2Vars):
                        raise CoreMcsUserError('''
    Parameters '%s' and '%s' cannot be correlated because
    they have a different number of random variables.''' % (corr.paramName, corr.otherName))

                    varNumPairs = map(lambda pair: (pair[0].rv.getVarNum(),
                                                    pair[1].rv.getVarNum()),
                                      zip(p1Vars, p2Vars))

                # In either case, use the pairs of varNums to set up the matrix
                for i, j in varNumPairs:
                    corrMat[i][j] = corrMat[j][i] = coef

        return corrMat

    @classmethod
    def finishSetup(cls):
        for obj in cls.instances:
            obj.other = XMLParameter.getInstance(obj.otherName)
            if obj.other is None:
                raise CoreMcsUserError('Unknown parameter "%s" in Correlation for %s' % (obj.otherName, obj.paramName))


class XMLQuery(XMLWrapper):
    '''Wraps a <Query> element, which holds an xpath expression.'''
    def __init__(self, element):
        super(XMLQuery, self).__init__(element)
        self.xpath = element.text.strip()

    def getXPath(self):
        return self.xpath

    def runQuery(self, tree):
        '''
        Run an XPath query on the given tree and return a list of the elements found.
        '''
        xpath = self.getXPath()
        found = tree.xpath(xpath)
        return found


class XMLTrialData(XMLWrapper):
    '''
    An abstract class that provides the protocol required of classes that can produce
    trial data, i.e., XMLDistribution or XMLDataFile. (TBD: and perhaps XMLPythonFunc?)
    '''
    def __init__(self, element, param):
        super(XMLTrialData, self).__init__(element)
        self.param = param
        self.rv    = None

    def getMode(self):
        '''Get the containing parameter mode, one of {'direct', 'shared', 'independent'}'''
        return self.param.getMode()

    def isShared(self, mode=None):
        if not mode:
            mode = self.getMode()
        return mode == 'shared'

    def getApply(self):
        '''
        Default distro application is 'direct', i.e., directly set the element value.
        '''
        s = self.element.get('apply', 'direct')
        return s.lower()

    def isDirect(self, applyAs=None):
        if not applyAs:
            applyAs = self.getApply()
        return applyAs == 'direct'

    def isFactor(self, applyAs=None):
        if not applyAs:
            applyAs = self.getApply()
        return applyAs in ('mult', 'multiply')

    def isDelta(self, applyAs=None):
        if not applyAs:
            applyAs = self.getApply()
        return applyAs == 'add'

    def ppf(self, *args):
        raise CoreMcsSystemError('Called abstract "ppf" method of %s' % self.___class__.name)


class XMLDistribution(XMLTrialData):
    '''
    A wrapper around a <Distribution> element to provide some
    convenience functions.
    '''
    def __init__(self, element, param):
        super(XMLDistribution, self).__init__(element, param)

        self.modDict = defaultdict(lambda: None)
        self.argDict  = {}

        _logger.debug("Distro params: %s", element.items())

        # Separate args into modifiers and distribution arguments
        for key, val in element.items():
            key = key.lower()
            if key in DISTRO_MODIF_ATTRS:
                self.modDict[key] = val
            elif key not in DISTRO_META_ATTRS:
                self.argDict[key]  = float(val)    # these form the signature that identifies the distroElt function

        sig = DistroGen.signature(self.getName(), self.argDict.keys())
        gen = DistroGen.generator(sig)
        if gen is None:
            raise DistributionSpecError("Unknown distribution signature %s" % str(sig))

        self.rv = gen.makeRV(self.argDict)  # generate a frozen RV with the specified arguments

    def getName(self):
        'Get the name of the distribution family (Normal, Uniform, Beta, etc.)'
        s = self.element.get('name', None)
        return s.lower()

    def getKeys(self):
        return self.element.keys()

    def ppf(self, *args):
        '''
        ppf() takes a first arg that can be a scalar value (percentile) or a list
        of percentiles, which is how we use it in this application.
        '''
        return self.rv.ppf(*args)

#
# TBD: define the syntax for this, e.g.
# TBD:  <PythonFunc>mypkg.mymod.func</PythonFunc>
#
# - Parse this get pointer to func (see Runner.py)
# - Have ppf call the func, passing what? The func can
#   access parameters by calling XMLParameter.getInstances(),
#   or XMLParameter.getInstance(name) to get one by name
#
class XMLPythonFunc(XMLTrialData):
    def __init__(self, element, param):
        super(XMLPythonFunc, self).__init__(element, param)
        self.func = importFromDotSpec(element.text)

    def ppf(self, *args):
        '''
        Call the function specified in the XML file, passing in the arguments to
        the ppf(), which should be a list of percentile values for which to return
        values from whatever underlies this Python function.
        '''
        return self.func(self.param, *args)


class XMLDataFile(XMLTrialData):
    '''
    Holds DataFrames representing files already loaded, keyed by abs pathname.
    A single data file can hold vectors of values for multiple Parameters; this
    way the file is loaded only once. This can be used to load pre-generated
    trial data, e.g. exported from other software.
    '''
    cache = {}

    @classmethod
    def getData(cls, pathname):
        '''
        Return the DataFrame created by loading the file at pathname. Read the
        file if it's not already in the cache, otherwise, just return the stored DF.
        '''
        pathname = os.path.abspath(pathname)    # use canonical name for cache

        if pathname in cls.cache:
            return cls.cache[pathname]

        df = pd.read_table(pathname, sep='\t', index_col=0)
        df.reset_index(inplace=True)
        cls.cache[pathname] = df    # cache it
        return df

    def __init__(self, element, param):
        super(XMLDataFile, self).__init__(element, param)
        filename = self.getFilename()
        self.df  = self.getData(filename)

    def getFilename(self):
        return os.path.expanduser(self.element.text)

    def ppf(self, *args):
        'Pseudo-ppf that just returns a column of data from the cached dataframe.'
        name   = self.param.getName()
        count  = len(args[0])

        if name not in self.df:
            raise CoreMcsUserError("Variable '%s' was not found in '%s'" % self.getFilename())

        vector = self.df[name]

        if len(vector) < count:
            # get the smallest integer number of repeats of the
            # vector to meet or exceed the desired count
            repeats = int(ceil(float(count)/len(vector)))
            vector  = np.array(list(vector) * repeats)

        return vector[:count]


class XMLVariable(XMLWrapper):
    '''
    Simple variable that wraps an Element and provides methods to get, set,
    cache, and restore Element values. This is subclassed by XMLRandomVar
    to provide a ppf() function for generating samples.
    '''
    def __init__(self, element, param, varNum=None):
        super(XMLVariable, self).__init__(element)
        self.param       = param
        self.paramPath   = None
        self.storedValue = None      # cached float value

        # N.B. "is not None" is required because "not element" isn't guaranteed to work.
        if element is not None:      # For shared RVs, XMLParameter creates XMLRandomVar with element=None
            self.storeFloatValue()   # We refer to stored value when updating the XML tree for each trial

        # Save the column in the trailMatrix with our values. This is passed
        # in when XMLVariables are instantiated directly (for shared variables)
        # and set automatically during initialization of XMLRandomVariables.
        self.varNum = varNum


    def getVarNum(self):
        return self.varNum

    def setVarNum(self, varNum):
        self.varNum = varNum

    def getValue(self):
        return self.element.text

    def setValue(self, value):
        '''
        Set the value of the element stored in an XMLVariable, or if inCopy
        is true, in the cached copy of the element.
        '''
        elt = self.getElement()

        elt.text = str(value)

    def storeFloatValue(self):
        'Store the current value as a float'
        value = self.getValue()
        try:
            self.storedValue = float(value)
        except Exception:
            name = self.param.getName()
            raise CoreMcsSystemError("Value '%s' for parameter %s is not a float" % (value, name))

    def getFloatValue(self):
        return self.storedValue


class XMLRandomVar(XMLVariable):
    '''
    Stores pointer to an XMLDistribution (unless this is a shared RV).
    Provides access to values by trial number via valueFunc.
    '''
    instances = []

    @classmethod
    def saveInstance(cls, obj):
        # Our position in the RV list identifies our column in the trial matrix
        obj.setVarNum(len(cls.instances))
        cls.instances.append(obj)

    @classmethod
    def getInstances(cls):
        return cls.instances

    def __init__(self, element, param):
        super(XMLRandomVar, self).__init__(element, param)
        self.saveInstance(self)

    def getParameter(self):
        return self.param

    def ppf(self, *args):
        '''
        Pass-thru to the Distribution's ppf() method. Called by LHS.
        '''
        dataSrc = self.param.getDataSrc()
        assert dataSrc, 'Called ppf on shared XMLRandomVar (dataSrc is None)'
        return dataSrc.ppf(*args)


class XMLParameter(XMLWrapper):
    '''
    Stores information for a single parameter definition, whether a distribution,
    exogenous data, or a Python function. For <Distribution> and <DataFile>, there
    must be an accompanying <Query> node with an xpath expression. See DTD.
    '''

    # Store a list of all parameter instances for use in generating distributions
    instances = dict()

    @classmethod
    def saveInstance(cls, obj):
        name = obj.getName()
        if name in cls.instances:
            raise CoreMcsUserError("Error: attempt to redefine parameter '%s'" % name)

        cls.instances[name] = obj

    @classmethod
    def getInstances(cls):
        'Return a list of the stored XMLParameter objects'
        return cls.instances.values()

    @classmethod
    def getInstance(cls, name):
        'Find an XMLParameter instance by name'
        return cls.instances.get(name, None)

    @classmethod
    def applyTrial(cls, trialNum, matrix):
        '''
        Copy random values for the given trial from the matrix into this parameter's elements.
        '''
        instances = cls.getInstances()
        for param in instances:
            param.updateElements(trialNum, matrix)

    def __init__(self, element, tree=None):
        # TBD: Unclear why calling super didn't work...
        # super(XMLParameter, self).__init__(element)
        self.element = element
        self.tree    = tree

        XMLParameter.saveInstance(self)

        self.vars    = []    # the list of XMLVariable or XMLRandomVar wrapping Elements from query
        self.rv      = None  # stored here only if the distro is shared across Elements from query
        self.query   = None  # XMLQuery instance
        self.dataSrc = None  # A subclass of XMLTrialData instance

        children = filter(lambda elt: not elt.tag is ET.Comment, element.getchildren())
        maxChildren = 3
        assert len(children) <= maxChildren, \
            "<Parameter> cannot have more than %d children. (This means the DTD is broken.)" % maxChildren

        # Recognized subclasses of XMLTrialData
        subclassMap = {'Distribution' : XMLDistribution,
                       'DataFile'     : XMLDataFile,
                       'PythonFunc'   : XMLPythonFunc}

        for elt in children:
            tag = elt.tag

            if tag == QUERY_ELT_NAME:
                self.query = XMLQuery(elt)
            elif tag == CORRELATION_ELT_NAME:
                XMLCorrelation(elt, self)
            else:
                # Otherwise it's one of Distribution, DataFile, or PythonFunc, all
                # of which are subclasses of XMLTrialData with a ppf() method.
                assert tag in subclassMap, "Expected element to be one of %s, got '%s'" % \
                                           (subclassMap.keys(), tag)
                cls = subclassMap[tag]
                self.dataSrc = cls(elt, self)

    def getName(self):
        return self.element.get('name')

    def getMode(self):
        s = self.element.get('mode', 'shared')
        return s.lower()

    def hasPythonFunc(self):
        dataSrcElt = self.dataSrc.getElement()
        return dataSrcElt.tag == PYTHON_FUNC_ELT

    def getDataSrc(self):
        return self.dataSrc

    def getQuery(self):
        return self.query

    def setTree(self, tree):
        self.tree = tree

    def getTree(self):
        return self.tree

    def getVars(self):
        return self.vars

    def runQuery(self, tree):
        '''
        Run the XPath query defined for a parameter, save the set of elements found.
        '''
        assert self.query, 'Called XMLParameter.runQuery with no XMLQuery defined'

        # Query returns a list of Element instances or None
        elements = self.query.runQuery(tree)
        if elements is None or len(elements) == 0:
            raise CoreMcsUserError("XPath query '%s' returned nothing for parameter %s" % (self.query.getXPath(), self.getName()))

        _logger.debug("Param %s: %d elements found", self.getName(), len(elements))

        if self.dataSrc.isShared():
            self.rv = XMLRandomVar(None, self)   # Allocate an RV and have all variables share its varNum
            varNum = self.rv.getVarNum()
            vars = map(lambda elt: XMLVariable(elt, self, varNum=varNum), elements)
        else:
            # XMLRandomVar assigns varNum directly
            vars = map(lambda elt: XMLRandomVar(elt, self), elements)

        # Add these to the list since we might be called for multiple scenarios
        self.vars.extend(vars)


    def updateElements(self, trialNum, matrix):
        '''
        Update an element's text (assuming it's a number) by multiplying
        it by a factor, adding a delta, or substituting a given value.
        Save the values of all RVs to the database.
        '''
        # TODO: address this case
        if self.hasPythonFunc():
            return

        assert self.vars, "Called updateElements with no variables defined in self.vars"

        isFactor = self.dataSrc.isFactor()
        isDelta  = self.dataSrc.isDelta()

        # Update all elements referenced by our list of XMLVariables (or of their subclass, XMLRandomVar)
        for var in self.vars:
            if var.getElement() is None:    # Skip shared RVs, which don't point to an XML element
                _logger.debug("Skipping shared element for %s", var)
                continue

            originalValue = var.getFloatValue()  # apply factor and delta to cached, original value

            randomValue = matrix[trialNum, var.varNum]
            newValue = randomValue * originalValue if isFactor else \
                ((randomValue + originalValue) if isDelta else randomValue)

            # Set the value in the cached tree so it can be written to trial's local-xml dir
            var.setValue(newValue)


def trialRelativePath(relPath, prefix):
    """
    Convert a pathname that was relative to "exe" to be relative to
    "exe/../../trial-xml" instead.
    For example, "../input/gcam-data-system/aglu-xml/foo.xml"
    becomes "../../trial-xml/input/gcam-data-system/aglu-xml/foo.xml"
    """
    parentDir = '../'
    if not relPath.startswith(parentDir):
        raise CoreMcsUserError("trialRelativePath: expected path starting with '%s', got '%s'" % \
                               (parentDir, relPath))

    newPath = os.path.join(prefix, 'trial-xml', relPath[len(parentDir):])
    return newPath


class XMLRelFile(XMLFile):
    """
    A minor extension to XMLFile to store the original relative pathname
    that was indicated in the config file identifying this file.
    """
    def __init__(self, relPath, simId):
        from .util import getSimLocalXmlDir

        self.relPath = relPath

        scenarioDir = getSimLocalXmlDir(simId)
        absPath = os.path.abspath(os.path.join(scenarioDir, relPath))

        super(XMLRelFile, self).__init__(absPath)

    def getRelPath(self):
        return self.relPath

    def getAbsPath(self):
        return self.getFilename()


class XMLInputFile(XMLWrapper):
    """
    <InputFile> abstraction that represents the XML input file(s) that are associated
    with a given <ScenarioComponent> name. This is necessary because there may be
    different versions of the file for this name in different scenarios. Each instance
    of XMLInputFile holds a set of XMLRelFile instances which each represent an actual
    GCAM input file. Each of these may be referenced from multiple scenarios.
    """

    # Maps a relative filename to an XMLRelFile objects that holds the parsed XML.
    # Exists to ensure that each file is read only once. This is a class variable
    # because the same XML file may be referenced by multiple scenarios and we want
    # to edit each only once.
    xmlFileMap = {}

    @classmethod
    def getModifiedXMLFiles(cls):
        return cls.xmlFileMap.values()

    def __init__(self, element):
        super(XMLInputFile, self).__init__(element)
        self.parameters = OrderedDict()
        self.element = element
        self.pathMap = defaultdict(set)
        self.xmlFiles = []

        # Parameters are described once here, but may be applied to multiple files
        findAndSave(element, PARAM_ELT_NAME, XMLParameter, self.parameters)

    def getComponentName(self):
        """
        Return the name attribute of the element for this <InputFile>, which is a
        unique tag identifying a "logical" file, which may appear in different versions
        in various scenarios, depending on what those scenarios needed to modify.
        """
        name = self.element.get('name')
        return name

    def mergeParameters(self, element):
        '''
        Save the parameters associated with the given <InputFile> element within
        self since it holds additional definitions for the same file. This allows
        a single input file to be referenced by multiple groups of parameters.
        '''
        eltName = element.get('name')
        compName = self.getComponentName()
        assert compName == eltName, \
            "mergeParameters: InputFile name mismatch: %s and %s" % (compName, eltName)

        findAndSave(element,  PARAM_ELT_NAME, XMLParameter, self.parameters)

    def loadFiles(self, simId, scenNames):
        """
        Find the distinct pathnames associated with our component name. Each scenario
        that refers to this path is stored in a set in self.inputFiles, keyed by pathname.
        The point is to read and update the target file only once, even if referred to
        multiple times.
        """
        compName = self.getComponentName()  # an identifier in the config file, e.g., "land_3"

        for scenName in scenNames:
            configFile = XMLConfigFile.getConfigForScenario(simId, scenName)
            relPath = configFile.getComponentPathname(compName)

            # If another scenario "registered" this XML file, we don't do so again.
            if not relPath in self.xmlFileMap:
                xmlFile = XMLRelFile(relPath, simId)
                self.xmlFileMap[relPath] = xmlFile  # unique for all scenarios so we read once
                self.xmlFiles.append(xmlFile)       # per input file in one scenario

            # TBD: In either case, we need to update the config files' XML trees because
            # TBD: some parameter(s) modify the file for this component, in all cases.
            # TBD: This new path has to be coordinated between config file and actual file.
            trialRelPath = trialRelativePath(relPath, '../..')
            configFile.updateComponentPathname(compName, trialRelPath)

    def runQueries(self):
        """
        Run the queries for all the parameters in this <InputFile> on each of
        the physical XMLRelFiles associated with this XMLInputFile.
        """
        tuples = []

        for param in self.parameters.itervalues():
            if param.hasPythonFunc():
                _logger.warn("Parameter '%s' defined unsupported python function." % param.getComponentName())
                continue                            # TODO: wire this in

            # Run the query on all the files associated with this abstract InputFile
            for xmlFile in self.xmlFiles:
                tree = xmlFile.getTree()
                param.runQuery(tree)        # accumulates elements from multiple files

            # Save variable names in the DB
            pname = param.getName()
            desc  = 'Auto-generated parameter'
            tuples.append((pname, desc))

        db = getDatabase()
        #_logger.debug("Saving tuples to db: %s", tuples)
        db.saveParameterNames(tuples)


class XMLParameterFile(XMLFile):
    """
    XMLParameterFile manipulation class.
    """
    def __init__(self, filename):
        super(XMLParameterFile, self).__init__(filename, load=True)

        # XMLAbstractInputFiles keyed by scenario component name
        self.inputFiles = OrderedDict()

        # Can't use "findAndSave" since we need to append if a filename is repeated
        inputFiles = self.inputFiles
        for elt in self.tree.iterfind(INFILE_ELT_NAME):
            compName = elt.get('name')
            if compName in inputFiles:
                inputFileObj = inputFiles[compName]
                inputFileObj.mergeParameters(elt)
            else:
                inputFiles[compName] = XMLInputFile(elt)

        # Match all the Correlation "with" names to actual parameter definitions
        XMLCorrelation.finishSetup()

        _logger.debug("Loaded parameter file: %s", filename)

    def getDTD(self):
        return PARAMETER_DTD

    def loadInputFiles(self, simId, scenNames):
        '''
        Load the input files, for each scenario in scenNames. Scenarios are
        found in {simDir}/sim-xml/{scenName}.
        '''
        for inputFile in self.inputFiles.values():
            inputFile.loadFiles(simId, scenNames)

        # Writes all modified configs to {simDir}/configs/{scenName}/config.xml
        # Config files' XML trees are updated as InputFile elements are processed.
        XMLConfigFile.writeAll(simId)

    def getFilename(self):
        return self.filename

    def getTree(self):
        """Returns XML parse tree."""
        return self.tree

    def runQueries(self):
        for obj in self.inputFiles.itervalues():
            obj.runQueries()

    def writeLocalXmlFiles(self, trialDir):
        """
        Write copies of all modified XML files
        """
        for xmlFile in XMLInputFile.getModifiedXMLFiles():
            exeRelPath = xmlFile.getRelPath()
            absPath = trialRelativePath(exeRelPath, trialDir)

            # Ensure that directories down to basename exist
            dirname = os.path.dirname(absPath)
            mkdirs(dirname)

            if os.path.exists(absPath):        # remove it since it might be a symlink and
                _logger.debug("Removing %s", absPath)
                os.unlink(absPath)             # we don't want to write through to the src

            _logger.debug("XMLParameterFile: writing %s", absPath)
            xmlFile.tree.write(absPath, xml_declaration=True, pretty_print=True)

    def dump(self):
        print "Parameter file: %s" % self.getFilename()
        for obj in self.inputFiles.itervalues():
            obj.dump()
